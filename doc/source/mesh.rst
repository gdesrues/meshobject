MeshObject
=====================================


.. class:: MeshObject.object.Mesh

	Alias of ``MeshObject.object.Object``


.. autoclass:: MeshObject.Object
	:members:
	:special-members:
	:undoc-members:
	:exclude-members: __hash__, __dict__, __module__, __weakref__, __module__, __dataclass_params__, __annotations__, __dataclass_fields__


.. autoclass:: MeshObject.object.PairIdCoord
	:members:
	:special-members:
	:undoc-members:
	:exclude-members: __hash__, __dict__, __module__, __weakref__, __repr__, __module__, __eq__, __dataclass_params__, __annotations__, __dataclass_fields__

