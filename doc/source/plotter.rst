MeshObject.Plotter
=====================================



Animations: See `this example <https://gitlab.inria.fr/gdesrues1/meshobject/-/blob/master/examples/animation.py>`_.

.. image:: /_static/anim_gif.gif

To plot an animation: `see here <https://gitlab.inria.fr/gdesrues1/meshobject/-/blob/master/examples/animation.py#L41-48>`_.

To save an animation to a mp4 file: `see here <https://gitlab.inria.fr/gdesrues1/meshobject/-/blob/master/examples/animation.py#L51-76>`_.


.. autoclass:: MeshObject.bindvista.plotter.Plotter
	:members:

.. autoclass:: MeshObject.bindvista.plotter.Themes
	:members:



