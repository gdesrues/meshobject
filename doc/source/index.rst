MeshObject
=====================================

Online documentation: `https://meshobject.doc.kerga.fr <https://meshobject.doc.kerga.fr>`_.

Git repo: `https://gitlab.inria.fr/gdesrues1/meshobject <https://gitlab.inria.fr/gdesrues1/meshobject>`_.


.. toctree::
	:maxdepth: 2

	mesh
	wrapping
	plotter
	prefabs



