import logging
import shutil
import unittest

import numpy as np
import treefiles as tf

from MeshObject import Mesh


class TestMeshing(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.d = tf.f(__file__, "data")
        self.d.file("sphere.vtk", "final.vtk")
        self.clear()
        self.m = Mesh.Sphere(phi_res=20, theta_res=20)

    @staticmethod
    def clear():
        tf.f(__file__, "data").dump(clean=True)

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(tf.f(__file__) / "data")

    def test_tetrahedralize(self):
        m = self.m.copy()
        m.write(self.d.sphere)
        m.tetrahedralize()
        m.write(self.d.final)

        r = Mesh.load(self.d.final)
        self.assertIsNotNone(r)

    def test_remesh(self):
        self.m.write(self.d.sphere)

        o = Mesh.load(self.d.sphere)
        mmg_opt = o.mmg_options(hmin=0.1, hmax=0.1, nr=True)
        o.tetrahedralize(mmg_opt)
        o.write(self.d.final)

        m = o.copy()
        pts = m.points.as_np()
        sol = pts[:, 0] + 2
        sol[np.where(pts[:, 0] < 0)] = 0.05
        m.remesh(sol)
        m.write(self.d.final)

        self.assertEqual(m.nbPoints, 4707)

    # def test_tetrahedralize_xml(self):
    #     Writer(self.vtk_sphere, connection=self.source.GetOutputPort(), type="xmlpd")
    #     Tetrahedralize(self.vtk_sphere, self.vtk_out, mmg=False)
    #     self.assertGreater(
    #         Mesh.load(self.vtk_sphere, type="xmlpd").points.as_np().shape[0], 0
    #     )

    def test_add_point_data(self):
        sphere = self.m.copy()
        pts = sphere.points.as_np()
        sphere.addPointData(pts[:, 0], "x")
        sphere.addPointData(pts[:, 2], "z")
        sphere.write(self.d.final)

        obj = Mesh.read(self.d.final)
        self.assertEqual(obj.data.GetPointData().GetArrayName(2), "z")

    def test_tetra_interpolate(self):
        m = self.m.copy()

        m.tetrahedralize(
            mmg=False,
            kernel=dict(
                name="vtkGaussianKernel",
                SetKernelFootprintToNClosest=[],
            ),
            volume=False,
            passPointData=True,
        )

        m.write(self.d.final)

    def test_interpolate(self):
        m = self.m.copy()
        m.clear()
        m.convertToPolyData()
        m.addPointData(m.pts[:, 2], "z")

        mt = m.copy()
        mt.tetrahedralize()
        mt.interpolate(m, volume=False, inside_val=-1)

        with tf.PvPlot(shape=(1, 2)) as p:
            k = dict(show_edges=True, scalars="z")
            p.subplot(0, 0)
            p.add_mesh(m.pv.clip(), **k)
            p.subplot(0, 1)
            p.add_mesh(mt.pv.clip(), **k)

        # m.write(self.d / "before.vtk")
        # mt.write(self.d / "after.vtk")

    # def test_addCell(self):
    #     Writer(self.vtk_sphere, connection=self.source.GetOutputPort(), type="xmlpd")
    #     Tetrahedralize(self.vtk_sphere, self.vtk_out, mmg=False)
    #
    #     sphere = Object.load(self.vtk_out)
    #     sphere.addCellData(np.zeros(sphere.nbCells), "test")
    #     sphere.addCell(15, test=12)
    #     self.assertEqual(sphere.getCellDataArray("test")[-1], 12)


log = logging.getLogger(__name__)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    log = tf.get_logger()

    unittest.main()
