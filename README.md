# MeshObject: a vtk wrapper


## Documentation

Follow [this link](https://meshobject.doc.kerga.fr).

## Cite

```
@software{MeshObjectPython,
  author = {Desrues, Gaëtan},
  title = {{3D mesh analysis and visualisation, VTK helper, and more}},
  url = {https://gitlab.inria.fr/gdesrues1/meshobject},
  version = {1.95.0},
  year = {2022}
}
```

## Examples
Go to [the examples directory](https://gitlab.inria.fr/gdesrues1/meshobject/-/tree/master/examples). You can start with [this notebook](https://gitlab.inria.fr/gdesrues1/meshobject/-/blob/master/examples/notebooks/01_misc.ipynb).

## Install
```
pip install MeshObject
```
or
```
pip install git+https://gitlab.inria.fr/gdesrues1/meshobject
```
or
```
git clone git@gitlab.inria.fr:gdesrues1/meshobject.git MeshObject && cd MeshObject
pip install -e .
python test_install.py
```




## Optional dependencies:
- skimage (for `Object.marching_cubes`)
- pyvista (not optional anymore)
- cardiac_utils (for SpatialData mainly, from cardiac-epyone by Nicoco)
- point_cloud_utils (for `Object.getVoronoi_Lloyd`)
- gmsh (for `Object.tetrahedralize` and `Object.triangulate`)




**Error in windows**: `Win32 not valid application` --> set `shell=True` in each `subprocess.check_output` call in Tetrahedralize.