import logging

import numpy as np
import treefiles as tf

from MeshObject import Mesh, Plotter


def main():
    m = Mesh.Sphere(theta_res=20, phi_res=20)
    m.addPointData(m.pts[:, 2], "z")

    mt = m.copy()
    mmg = m.mmg_options(hmax=0.15, hmin=0.15, hgrad=1, nr=True)
    mt.tetrahedralize(mmg)
    mt.interpolate(m, volume=False, inside_val=-1)
    mt.addPointData(mt.pts[:, 0], "x")
    mt.threshold((-1, 0.25), "x", to_polydata=False)

    me = m.copy()
    me.threshold((-1, 0.25), "z")
    bedges = me.get_boundary_edges()
    mep, pts = me.pts, []
    for hole in bedges:
        for edge in hole:
            pts.append(mep[edge[0]])

    mes = Mesh.from_boundary_edges(bedges, me)
    title = "Mesh generation, tetrahedrisation, interpolation, clipping boundary edges"
    with Plotter(shape=(1, 3), title=title, size=(1600, 700)) as p:
        k = dict(show_edges=True, scalars="z")
        p[0, 0].add_mesh(m.pv.clip(), **k)
        p[0, 1].add_mesh(mt, **k)
        p[0, 2].add_mesh(me, **k)
        p.add_points(np.array(pts), color="red")
        p.add_mesh(mes, radius=0.01, color="green")
        p.link_views()


log = logging.getLogger(__name__)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    log = tf.get_logger()

    main()
