import logging
import os
import platform
import shutil
import subprocess
import sys
from typing import List

import treefiles as tf
import vtk
from MeshObject import Object, TMeshLoadable
from MeshObject.convert_old_style import convert_to_old_format
from MeshObject.mmg_options import MMGOptions
from MeshObject.vtk_wrap import Reader, Writer

# This module requires gmsh (https://gmsh.info/) and mmg (https://www.mmgtools.org/)
# mmg executables are included for windows, linux and mac for 3d and surface remeshing

GMSH = tf.curDirs(sys.executable, "gmsh")
if not tf.isfile(GMSH):
    GMSH_1 = shutil.which("gmsh")
    GMSH_2 = os.environ.get("GMSH_PATH")
    GMSH = GMSH_1 or GMSH_2
    if GMSH is None:
        raise ModuleNotFoundError(
            "'gmsh' was not found!\nYou can install gmsh with `pip install gmsh-sdk` (see "
            "https://github.com/mrkwjc/gmsh-sdk) "
        )

ssw = {}
if platform.system() == "Windows":
    ssw = {"shell": True}


@tf.timer
def Tetrahedralize(
    _polydata,
    _unstructuredgrid,
    mmg=True,
    mmg_opt: [List, MMGOptions] = None,
    passPointData=False,
    **kwargs,
):
    """
    Will tetrahedralize the mesh given as `_polydata` and produce and
    unstructured mesh, wrote to filename `_unstructuredgrid`.

    :param str _polydata: Input mesh, an object-loadable polydata
    :param str _unstructuredgrid: Output unstructured mesh filename
    :param bool mmg: Choose to remesh with mmg
    :param list mmg_opt: mmg options, see https://www.mmgtools.org/mmg-remesher-try-mmg/mmg-remesher-options
                         - hausd: relative to the object size
                         - hgrad: gradation of edges lenght
    :param bool passPointData: Interpolate PointData, see <vtk_wrap.Interpolate>.
    :param kwargs: See <vtk_wrap.Interpolate>
    """
    # If input polydata is not a gmsh readable format, ie ascii polydata legacy .vtk file
    isPath, isVtk, isXml = type(_polydata) is str, True, False
    if isPath:
        isVtk = os.path.splitext(_polydata)[1] == ".vtk"
        with open(_polydata, "rb") as f:
            isXml = b"<" in f.readline()
    CREATE_TEMP_MESH = not isPath or isXml or not isVtk

    tmpfile = None
    if CREATE_TEMP_MESH:
        tmpfile = tf.TmpFile(suffix=".vtk")
        mesh = tmpfile.enter()
        if isXml:
            log.debug("Spotted a xml file, converting to regular polydata")
            Writer(mesh.name, data=Reader(_polydata, type="xmlpd").data, type="pd")
        elif not isVtk:
            Writer(mesh.name, data=Reader(_polydata).data, type="pd")
        elif type(_polydata) is vtk.vtkPolyData:
            Writer(mesh.name, data=_polydata, type="pd")
        elif isinstance(_polydata, Object):
            _polydata.write(mesh.name, type="pd")
        else:
            log.error("Connot understand polydata argument: " + str(_polydata))
        _polydata = mesh.name

    try:
        convert_to_old_format(_polydata)
        # (Object.load(_polydata)).convert_to_old_format(_polydata)
        log.info("Converting to old-style vtk data file version")
    except Exception as e:
        log.error(f"Could not convert to old-style vtk data file: {e}")
        pass

    # Set default remeshing option:
    if mmg_opt is None:
        mmg_opt = MMGOptions(_polydata, hgrad=1)

    # Set file names
    root = os.path.splitext(_unstructuredgrid)[0]
    gmsh_out = root + "_gmsh_out" + ".mesh"
    mmg_out = root + "_mmg_out" + ".mesh"

    # A geo file is required to create a volume for gmsh. Here we assume there
    # is only one surface. Mesh is called with the 'Merge' directive
    geo_template = f"Merge {_polydata!r};"
    # \nMesh.CharacteristicLengthMin = 1;\nMesh.CharacteristicLengthMax = 2;
    # geo_template += "Mesh.AngleToleranceFacetOverlap=0.05;\n"
    geo_template += "\nSurface Loop(1) = {1};\n//+\nVolume(1) = {1};"

    with tf.TmpFile(suffix=".geo") as f:
        f.write(geo_template)
        f.flush()

        # We have a valid surface mesh and a geo file, let's create the volumetric mesh
        log.debug("Start meshing")
        std_out = subprocess.check_output([GMSH, f.name, "-3", "-o", gmsh_out], **ssw)
        log.debug(std_out)
        log.info("Meshing with gmsh ok")

    if not mmg:
        mmg_out = gmsh_out
    else:
        remesh(_in=gmsh_out, _out=mmg_out, mmg_opt=mmg_opt, method="volume")

    log.debug("Exporting to vtk")
    std_out = subprocess.check_output(
        [GMSH, mmg_out, "-o", _unstructuredgrid, "-save"], **ssw
    )
    log.debug(std_out)

    if passPointData:
        ugrid = Object.load(_unstructuredgrid)
        ugrid.interpolate(_polydata, fname=_unstructuredgrid, **kwargs)

    if CREATE_TEMP_MESH:
        tmpfile.exit()

    # Check if it ok
    if Object.load(_unstructuredgrid).nbPoints == 0:
        log.error(
            f"Tetrahedralization may have failed"  # , check log file file://{LOG_FILE}"
        )
    else:
        log.info(f"Tetrahedralization ok, see {_unstructuredgrid!r}")

    tf.removeIfExists(gmsh_out)
    tf.removeIfExists(mmg_out)


def remesh(_in, _out, mmg_opt: [List, MMGOptions], method):
    """
    Wrapper to use mmg
    """
    if method == "volume":
        soft = "mmg3d_O3"
    elif method == "surface":
        soft = "mmgs_O3"
    else:
        msg = "<remesh> Choose method between 'surface' and 'volume'"
        raise KeyError(msg)

    softs = {
        "Windows": ".exe",
        "Darwin": "_osx",
        "Linux": "",
    }
    s = platform.system()
    if s not in softs:
        log.error(f"OS not recognized: {s!r}")
        raise RuntimeError(f"OS not recognized: {s!r}")
    soft = tf.f(__file__, "mmg", "5.6.0") / soft + softs[s]

    if isinstance(mmg_opt, MMGOptions):
        mmg_opt = mmg_opt.opt

    # if "-sol" not in mmg_opt:
    #     x = re.search(r"Vertices (\d+)", tf.load_str(_in).replace("\n", ""))
    #     solfname = os.path.splitext(_out)[0] + ".sol"
    #     sol = "\n".join(np.ones(int(x.group(1)), dtype=str))
    #     sol_tmpl = (
    #         "MeshVersionFormatted 2\n"
    #         "Dimension 3\n"
    #         f"SolAtVertices {x.group(1)}\n"
    #         f"1 1\n"
    #         f"{sol}\n"
    #         "End"
    #     )
    #     tf.dump_str(solfname, sol_tmpl)
    #     mmg_opt += ["-sol", solfname]
    # else:
    #     solfname = mmg_opt[mmg_opt.index("-sol") + 1]

    log.debug("Start remeshing")
    std_out = subprocess.check_output(
        [
            soft,
            "-in",
            _in,
            "-out",
            _out,
            *list(map(str, mmg_opt)),
        ],
        **ssw,
    )
    log.debug(std_out)
    # tf.removeIfExists(solfname)
    log.info(f"Remeshing with mmg ok")


@tf.timer
def Triangulate(
    _input: TMeshLoadable,
    output: TMeshLoadable,
    mmg=True,
    mmg_opt: [List, MMGOptions] = None,
):
    """
    Will triangulate the mesh given as `_input` and produce and
    unstructured mesh, wrote to filename `output`.

    :param str _input: Input mesh, object_loadable polydata
    :param str output: Output unstructured mesh filename
    :param bool mmg: Choose to remesh with mmg
    :param list mmg_opt: mmg options, see https://www.mmgtools.org/mmg-remesher-try-mmg/mmg-remesher-options
    """
    # Set default remeshing option:
    if mmg_opt is None:
        # mmg_opt = []
        mmg_opt = MMGOptions(_input, hgrad=1)
        # you should also set "-hmax"

    # If _input polydata is not a gmsh readable format, ie ascii polydata legacy .vtk file
    isPath, isVtk, isXml = type(_input) is str, True, False
    if isPath:
        isVtk = os.path.splitext(_input)[1] == "vtk"
        with open(_input, "rb") as f:
            isXml = b"<" in f.readline()
    CREATE_TEMP_MESH = not isPath or isXml or not isVtk

    tmpfile = None
    if CREATE_TEMP_MESH:
        tmpfile = tf.TmpFile(suffix=".vtk")
        mesh = tmpfile.enter()
        if isXml:
            log.debug("Spotted a xml file, converting to regular polydata")
            Writer(mesh.name, data=Reader(_input, type="xmlpd").data, type="pd")
        elif not isVtk:
            Writer(mesh.name, data=Reader(_input).data, type="pd")
        elif type(_input) is vtk.vtkPolyData:
            Writer(mesh.name, data=_input, type="pd")
        elif isinstance(_input, Object):
            _input.write(mesh.name, type="pd")
        else:
            log.error("Connot understand _input argument: " + str(_input))
        _input = mesh.name

    try:
        # (Object.load(_input)).replace_polygons(_input)
        convert_to_old_format(_input)
        log.info("Converting to old-style vtk data file version")
    except Exception as e:
        log.error(f"Could not convert to old-style vtk data file: {e}")
        pass

    # Set file names
    root = os.path.splitext(output)[0]
    gmsh_out = root + "_gmsh_out" + ".mesh"
    mmg_out = root + "_mmg_out" + ".mesh"

    # A geo file is required to create a volume for gmsh. Here we assume there
    # is only one surface. Mesh is called with the 'Merge' directive
    geo_template = f"Merge {_input!r};"
    geo_template += "\nSurface Loop(1) = {1};"

    with tf.TmpFile(suffix=".geo") as f:
        f.write(geo_template)
        f.flush()

        # We have a valid surface mesh and a geo file, let's create the volumetric mesh
        std_out = subprocess.check_output(
            [GMSH, f.name, "-o", gmsh_out, "-save"], **ssw
        )
        log.debug(std_out)

    if not mmg:
        mmg_out = gmsh_out
    else:
        remesh(_in=gmsh_out, _out=mmg_out, mmg_opt=mmg_opt, method="surface")

    subprocess.check_output([GMSH, mmg_out, "-o", output, "-save"], **ssw)
    log.debug(std_out)

    if CREATE_TEMP_MESH:
        tmpfile.exit()

    tf.removeIfExists(gmsh_out)
    tf.removeIfExists(mmg_out)

    # Check if it ok
    out_ = Object.load(output)
    out_.convertToPolyData()
    out_.write(output)
    if out_.nbPoints == 0:
        log.error(
            f"Triangulation may have failed"
        )  # , check log file file://{LOG_FILE}")
    else:
        log.info(f"Triangulation ok, see {output!r}")
        # print(f"file://{output}")
        # breakpoint()


log = logging.getLogger(__name__)
