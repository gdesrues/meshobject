import logging

import numpy as np
import treefiles as tf

from MeshObject import Mesh, Plotter

"""
Select some points on a mesh with pyvista cell selection tools
"""


def select_points(
    mesh: Mesh, scalars: str, new_label: float = None, through=False, cmap="jet_r"
):
    all_picked_points = []
    show_message = f"Press 'r' and select points"
    new_label = tf.none(new_label, np.nan)

    def callback(picked_points, plotter, mesh, **_):
        all_picked_points.extend(picked_points)
        scals = mesh[scalars]
        scals[all_picked_points] = new_label
        plotter.update_scalars(scals, mesh=mesh)

    with Plotter(title="Points selection") as p:
        mpv = mesh.pv

        def cb(picked_cells):
            if picked_cells:
                picked_points = [
                    mesh.closestPoint(pt).idx for pt in picked_cells.points.tolist()
                ]
                callback(picked_points, p, mpv)

        p.add_mesh(mpv, scalars=scalars, cmap=cmap)
        p.enable_cell_picking(
            show_message=show_message,
            font_size=13,
            callback=cb,
            through=through,
            style="points",
            color="red",
        )

    return np.unique(all_picked_points)


log = logging.getLogger(__name__)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    log = tf.get_logger()

    main()
