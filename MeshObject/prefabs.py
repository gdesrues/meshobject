import logging
import numpy as np
import vtk
from vtkmodules.vtkCommonComputationalGeometry import vtkParametricEllipsoid
from vtkmodules.vtkCommonTransforms import vtkLandmarkTransform
from vtkmodules.vtkFiltersSources import (
    vtkCubeSource,
    vtkCylinderSource,
    vtkSphereSource,
    vtkParametricFunctionSource,
    vtkSuperquadricSource,
    vtkPlaneSource,
)


def BuildLandmarkTransform(sourcePoints, targetPoints, rigid=False):
    """
    BuildLandmarkTransform

    See the `documentation <https://vtk.org/doc/nightly/html/classvtkLandmarkTransform.html>`_.
    """
    landmark_transform = vtkLandmarkTransform()
    landmark_transform.SetSourceLandmarks(sourcePoints)
    landmark_transform.SetTargetLandmarks(targetPoints)
    if rigid:
        landmark_transform.SetModeToRigidBody()  # Default is SetModeToSimilarity
    landmark_transform.Modified()
    landmark_transform.Update()
    return landmark_transform


def BuildTransform(
    vec1=None, vec2=None, translate=None, rotate=None, scale=1, post_multiply=True
):
    """
    Create a vtk transformation matrix. Can build the transform from two vectors.

    :param Coord vec1: Initial vector for rotation
    :param Coord vec2: Terminal vector for rotation
    :param list translate: Translation
    :param list rotate: Rotation in orthonormale basis, in degrees
    :param scale: Scaling factor (number for isotropic and list for anisotropic)
    :param post_multiply: see doc: https://vtk.org/doc/nightly/html/classvtkTransform.html
    """
    if rotate is None:
        rotate = [0, 0, 0]
    if translate is None:
        translate = [0, 0, 0]
    if isinstance(scale, (list, tuple)):
        scale = np.asarray(scale)
    if not isinstance(scale, np.ndarray):
        scale = np.array([scale] * 3)

    transform = vtk.vtkTransform()
    transform.Translate(translate)
    transform.RotateX(rotate[0])
    transform.RotateY(rotate[1])
    transform.RotateZ(rotate[2])
    transform.Scale(scale)

    if vec1 is not None and vec2 is not None:
        norm = lambda x: np.asarray(x) / np.linalg.norm(x)
        vec1, vec2 = norm(vec1), norm(vec2)
        v = np.cross(vec1, vec2)
        a = np.arccos(np.dot(vec1, vec2))
        if post_multiply:
            transform.PostMultiply()
        transform.RotateWXYZ(np.rad2deg(a), v)

    # log.debug(f"Transform matrix: {transform.GetMatrix()}")

    return transform


def Source(dtype, **kwargs):
    """
    Template to create sphere and cylinder sources
    """
    vtk_object = dtype()

    for k, v in kwargs.items():
        if not isinstance(v, list):
            v = [v]
        getattr(vtk_object, k)(*v)

    vtk_object.Update()
    return vtk_object.GetOutput()


def Sphere(**kwargs):
    """
    Create a vtk sphere polydata.

    See the `documentation <https://vtk.org/doc/nightly/html/classvtkSphereSource.html>`_.

    For example:

    :param float SetPhiResolution:
    :param float SetThetaResolution:
    :param Coord SetCenter:
    """
    return Source(vtkSphereSource, **kwargs)


def Plane(**kwargs):
    """
    Create a vtk Plane polydata.

    See the `documentation <https://vtk.org/doc/nightly/html/classvtkPlaneSource.html>`_.
    """
    return Source(vtkPlaneSource, **kwargs)


def Cylinder(**kwargs):
    """
    Create a vtk Cylinder polydata.

    See the `documentation <https://vtk.org/doc/nightly/html/classvtkCylinderSource.html>`_.
    """
    return Source(vtkCylinderSource, **kwargs)


def Cube(**kwargs):
    """
    Create a vtk Cube polydata.

    See the `documentation <https://vtk.org/doc/nightly/html/classvtkCubeSource.html>`_.
    """
    return Source(vtkCubeSource, **kwargs)


# def Plotter(*meshes):
#     """
#     Helper to plot some meshes
#
#     Each item is a dict with keys:
#
#     - mesh: vtk object
#     - style: style options
#     - A lot more: see the `Pyvista doc
#     <https://docs.pyvista.org/plotting/plotting.html#pyvista.BasePlotter.add_mesh>`_
#     """
#     try:
#         import pyvista as pv
#     except ModuleNotFoundError:
#         raise ModuleNotFoundError(
#             "Package 'pyvista' not found, visit https://docs.pyvista.org/getting-started/installation.html"
#         )
#
#     plotter = pv.Plotter()
#     for m in meshes:
#         plotter.add_mesh(**m)
#     return plotter


def Ellipsoid(radiuses):
    """
    Create an ellipsoid.

    See the `documentation <https://vtk.org/doc/nightly/html/classvtkParametricEllipsoid.html>`_.
    """
    ellipsoid = vtkParametricEllipsoid()
    ellipsoid.SetXRadius(radiuses[0])
    ellipsoid.SetYRadius(radiuses[1])
    ellipsoid.SetZRadius(radiuses[2])

    source = vtkParametricFunctionSource()
    # source.SetWResolution(100)
    source.SetParametricFunction(ellipsoid)
    source.Update()

    return source.GetOutput()


def Superquadric(**kwargs):
    """
    Create a vtk Superquadric polydata.

    See the `documentation <https://vtk.org/doc/nightly/html/classvtkSuperquadricSource.html>`_.
    """
    return Source(vtkSuperquadricSource, **kwargs)


log = logging.getLogger(__name__)
