import logging
from typing import TypeVar

import numpy as np
import pyvista
import treefiles as tf
from MeshAnim.time_series import TimeSeries

from MeshObject import Mesh
from bindvista.plotter import Themes, Plotter

T = TypeVar("T", bound="PvInter")


# 3 classes required:
# - Plotter: application launcher & context
# - BaseWindow(MainWindow): the qt main window
# - PvInter(QtInteractor): pyvista interactor (widget within the window's layout)


log = logging.getLogger(__name__)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    log = tf.get_logger()

    m = Mesh.Sphere(phi_res=30, theta_res=30)
    m["z"] = m.pts[:, 2]

    ml = Mesh.from_elements([(0, 0, 0), (0, 1, 1)], [(0, 1)])
    # print(ml)

    # m2 = Mesh.Sphere()
    # m2.tetrahedralize(m2.mmg_options(hmin=0.1, hmax=0.1, nr=True))
    # m2["z"] = m2.pts[:, 2]

    ro = tf.f(__file__, "out").dump()
    m["t"] = tf.minmax_(m["z"])
    for i in range(20):
        r = m.copy()
        r.points = r.pts + 0.2 * i
        r["t"] = np.ones(r.nbPoints) * i * 5e-3
        r.write(ro / f"s_{i}.vtk")

    ts = TimeSeries.from_glob(ro / "s_*.vtk")

    # with _PL(theme=tms.NUM, title="test", shape="1|2", size=(1400, 800)) as p:
    with Plotter(theme=Themes.NUM, title="test") as p:
        p.add_mesh(m, color="g", style="wireframe")
        # p.add_mesh(m, scalars="z", show_scalar_bar=False)

        mo = Mesh.load(tf.f(__file__) / "rsc/data/thorax_head.obj")
        mo.transform(rotate=(15, 15, 15))
        p.add_orientation_widget(mo, color="#C6C6C6")

        # p.add_point(
        #     coord=(0, 0.5, 0),
        #     label="me",
        #     point_size=40,
        #     point_color="r",
        #     text_color="w",
        #     margin=5,
        #     shape_color="k",
        # )

        # p.add_vector((0, 0, 2), p2=(0, 0, 1), scale=0.2)
        # m["vec"] = np.random.uniform(-1, 1, (m.nbPoints, 3))
        # p.add_vectors(m, "vec", scale=0.1, color="b")
        # p.add_vectors(m, vec=np.random.uniform(-1, 1, (m.nbPoints, 3)), scale=0.1, color='b')

        # # Plot mesh with lines
        # p.add_mesh(ml, radius=0.03)
        # # Plot one line
        # p.add_line((-1, -1, -1), (1, 1, 1), color="r")

        # add tetra with threshold
        # p.add_mesh_threshold(m2, "z")

        # # 2D plots
        # x = np.linspace(0, 10, 1000)
        # y1 = np.cos(x) + np.sin(3 * x)
        # y2 = 0.1 * (x - 5)
        # chart = pv.Chart2D()
        # chart.line(x, y1, color=(0.9, 0.1, 0.1), width=4, style="--")
        # chart.line(x, y2, color=(0.1, 0.9, 0.1), width=4, style="--")
        # # chart.title = "Volume"
        # chart.x_label = "Time (ms)"
        # chart.y_label = "Volume (mL)"
        # chart.background_color = "w"
        # p[1, 0].add_chart(chart)
        # p[2, 0].add_chart(chart)

        # Animations
        # p.add_ts(ts, scalars="t")
        # p.show_bounds()
