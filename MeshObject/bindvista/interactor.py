import logging
from typing import Union, TypeVar, Callable, List

import numpy as np
import pyvista as pv
import treefiles as tf
# from MeshAnim.time_series import TimeSeries
from pyvista import _vtk, get_array, get_array_association
from pyvistaqt import QtInteractor

from MeshObject import Mesh, TMeshLoadable

T = TypeVar("T", bound="PvInter")


class PvInter(QtInteractor):
    def __init__(self, set_anim: Callable = None, **kwargs):
        super().__init__(**kwargs)
        from MeshObject.bindvista.plotter import Themes

        self.theme = kwargs.pop("theme", Themes.DOC)
        if not kwargs.get("off_screen", False):
            self.set_key_events()
        self.set_anim = set_anim
        self.timeseries = {}

    def __getitem__(self, item) -> T:
        if isinstance(item, tuple):
            self.subplot(*item)
        return self

    def set_key_events(self):
        try:
            self.add_key_event("c", lambda: print(self.camera_position))
            self.add_key_event("r", self.reset_camera)
        except:
            pass

    @QtInteractor.theme.setter
    def theme(self, theme):
        pv.global_theme.load_theme(theme)
        self.background_color = theme.background
        QtInteractor.theme.fset(self, theme)

    def add_points(
        self,
        coords: Union[list, np.ndarray, TMeshLoadable] = None,
        labels=None,
        point_size=20,
        render_points_as_spheres=True,
        always_visible=True,
        **kwargs,
    ):
        kwargs.update(
            {
                "point_size": point_size,
                "render_points_as_spheres": render_points_as_spheres,
            }
        )
        if labels is not None:
            kwargs.update({"always_visible": always_visible})
            return self.add_point_labels(
                np.array(coords, dtype=float), labels, **kwargs
            )
        else:
            if isinstance(coords, Mesh):
                obj = coords.copy()
                m = pv.PolyData(obj.pts)
                for y in obj.pointDataNames:
                    m[y] = obj.getPointDataArray(y)
            elif isinstance(coords, pv.DataSet):
                m = coords
            else:
                m = pv.PolyData(np.array(coords, dtype=float))
            return self.add_mesh(m, **kwargs)

    def add_point(self, coord=None, label="", **kwargs):
        return self.add_points([coord], [label], **kwargs)

    def add_mesh(
        self,
        obj: Union[str, Mesh, pv.DataSet],
        vals=None,
        force_no_tube=False,
        **kwargs,
    ):
        if isinstance(obj, str):
            obj = Mesh.load(obj)
        if isinstance(obj, Mesh):
            if obj.nbPoints == 0:
                log.error(f"No points, no plotting")
                return

            # if not force_no_tube and obj.nbLines == obj.nbCells:  # Specialize for lines
            nbCells = obj.cells.as_np().shape[0]
            if not force_no_tube and obj.nbLines == nbCells:
                x = pv.PolyData(obj.pts)
                x.lines = np.column_stack(([2] * nbCells, obj.cells.as_np()))
                for y in obj.pointDataNames:
                    x[y] = obj.getPointDataArray(y)
                obj = x.tube(radius=kwargs.pop("radius", 0.1))
                kwargs.update({"show_edges": False, "render_lines_as_tubes": True})
            else:
                obj = obj.pv

        return super().add_mesh(obj, **kwargs)

    def add_vectors(
        self,
        obj: Union[Mesh, pv.DataSet, np.array],
        array_name: str = None,
        vec=None,
        # scale=None,
        glyph_kw=None,
        **kwargs,
    ):
        """
        Add glyph vector field
        :param obj: either a mesh or a list of point coordinates
        :param array_name: if obj is a dataset, get the vectors from the point data array
        :param vec: np.ndarray that contains the vectors directions
        # :param scale: scaling parameter  (use `factor`)
        """
        if isinstance(obj, Mesh):
            obj = obj.pv
        elif not pv.is_pyvista_dataset(obj):
            obj = pv.PolyData(obj)
        if vec is not None:
            array_name = "vec"
            obj["vec"] = np.array(vec)
        # if scale:
        #     obj[array_name] *= scale
        obj.set_active_vectors(array_name)
        glyph_kw = tf.none(glyph_kw, {})
        self.add_mesh(obj.glyph(**glyph_kw), **kwargs)

    def add_vector(self, p1, *, p2=None, direction=None, scale=None, **kwargs):
        if direction is None:
            direction = np.array(p2) - np.array(p1)
            # direction /= np.linalg.norm(direction)
        kwargs["show_edges"] = False
        self.add_mesh(pv.Arrow(start=p1, direction=direction, scale=scale), **kwargs)

    def add_line(self, p1, p2, **kwargs):
        self.add_mesh(Mesh.from_elements([p1, p2], [(0, 1)]), **kwargs)

    def add_mesh_threshold(
        self,
        obj,
        scalars=None,
        scalars_threshold=None,
        invert=False,
        widget_color=None,
        preference="cell",
        title=None,
        pointa=(0.4, 0.9),
        pointb=(0.9, 0.9),
        continuous=False,
        initial_value=None,
        style="modern",
        hide_value=False,
        **kwargs,
    ):
        # kwargs.update(
        #     {
        #         "invert": True,
        #         "pointa": (0.1, 0.9),
        #         "pointb": (0.6, 0.9),
        #         "widget_color": "k",
        #         # "scalars": scalars,
        #     }
        # )
        if isinstance(obj, Mesh):
            obj = obj.pv
        mesh = obj
        s2 = scalars_threshold

        """Modifying parent method to allow separate scalars for displaying and thresholding"""

        if isinstance(mesh, pv.MultiBlock):
            raise TypeError(
                "MultiBlock datasets are not supported for threshold widget."
            )
        name = kwargs.get("name", mesh.memory_address)
        if scalars is None:
            field, scalars = mesh.active_scalars_info
        arr = get_array(mesh, scalars, preference=preference)
        if arr is None:
            raise ValueError("No arrays present to threshold.")
        field = get_array_association(mesh, s2, preference=preference)

        rng = mesh.get_data_range(s2)
        kwargs.setdefault("clim", kwargs.pop("rng", rng))
        if title is None:
            title = s2
        mesh.set_active_scalars(scalars)

        self.add_mesh(mesh.outline(), name=name + "outline", opacity=0.0)

        alg = _vtk.vtkThreshold()
        alg.SetInputDataObject(mesh)
        alg.SetInputArrayToProcess(
            0, 0, 0, field.value, s2
        )  # args: (idx, port, connection, field, name)
        alg.SetUseContinuousCellRange(continuous)

        if not hasattr(self, "threshold_meshes"):
            self.threshold_meshes = []
        threshold_mesh = pv.wrap(alg.GetOutput())
        self.threshold_meshes.append(threshold_mesh)

        def callback(value):
            if invert:
                alg.ThresholdByLower(value)
            else:
                alg.ThresholdByUpper(value)
            alg.Update()
            threshold_mesh.shallow_copy(alg.GetOutput())

        self.add_slider_widget(
            callback=callback,
            rng=rng,
            title=title,
            color=widget_color,
            pointa=pointa,
            pointb=pointb,
            value=initial_value,
            style=style,
            fmt="" if hide_value else None,
        )

        kwargs.setdefault("reset_camera", False)
        self.add_mesh(threshold_mesh, scalars=scalars, **kwargs)

    def add_camera_orientation_widget(
        self, position=(0, 0.025, 0.15, 0.15), animate=True, n_frames=20
    ):
        ori = super().add_camera_orientation_widget(animate=animate, n_frames=n_frames)
        if isinstance(position, tuple):
            ori.GetDefaultRenderer().SetViewport(*position)

    def add_orientation_widget(self, obj, position=(0, 0.9, 0.15, 1), **kw):
        if isinstance(obj, Mesh):
            obj = obj.pv
        ori = super().add_orientation_widget(obj, **kw)
        if isinstance(position, tuple):
            ori.SetViewport(*position)

    def add_ts(self, ts: "TimeSeries", **kwargs):
        if "scalars" not in kwargs:
            name = tf.get_string()
            self.timeseries[name] = {"ts": ts, "sc": None}
            self.add_mesh(ts[0][1], name=name, **kwargs)
            self.set_anim(ts)
        else:
            name = f"{kwargs['scalars']}_{tf.get_string()}"
            self.timeseries[name] = {"ts": ts, "sc": kwargs["scalars"]}
            self.add_mesh(ts[0][1], name=name, **kwargs)
            self.set_anim(ts)

            rgs = np.array(
                [x["ts"].range(kwargs["scalars"]) for x in self.timeseries.values()]
            )
            self.update_scalar_bar_range((np.min(rgs[:, 0]), np.max(rgs[:, 1])))

    def updt_anim(self, n: int, maxn: int):
        # print("updt", n, maxn, self.timeseries)

        for k, v in self.timeseries.items():
            t, m = v["ts"][min(n, len(v["ts"]) - 1)]
            kw = dict(mesh=self.renderer.actors[k].GetMapper().GetInput(), render=False)
            self.update_coordinates(m.pts, **kw)
            try:
                if v["sc"] is not None:
                    self.update_scalars(m[v["sc"]], **kw)
            except:
                x = pv.PolyData(m.pts)
                x.lines = np.column_stack(([2] * m.nbCells, m.cells.as_np()))
                if v["sc"] is not None:
                    x[v["sc"]] = m.getPointDataArray(v["sc"])
                # print(self.renderer.actors[k].GetMapper().GetInput())
                self.add_mesh(
                    x.tube(radius=0.5),
                    name=k,
                    show_edges=False,
                    show_scalar_bar=False,
                    clim=None
                    if v["sc"] is None
                    else (0, 70),  # TODO: get scalarbar range
                )
                # self.update_scalar_bar_range(v["ts"].range(v["sc"]))
            else:
                self.add_mesh(
                    m,
                    scalars=v["sc"],
                    name=k,
                    clim=None
                    if v["sc"] is None
                    else (0, 70),  # TODO: get scalarbar range
                )

        # self.reset_camera()
        self.render()


log = logging.getLogger(__name__)
