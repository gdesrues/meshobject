import logging

import treefiles as tf
from MeshAnim.time_series import TimeSeries
from PyQt5.QtCore import QTimer, pyqtSignal, QObject
from PyQt5.QtGui import QIcon


class Animation(QObject):
    timeout = pyqtSignal(int, int)

    def __init__(self, ts: TimeSeries, interval=150):
        super().__init__()
        self.interval = interval
        self.ts = ts
        self.dt = tf.none(self.ts.dt, 0.1)
        self._timer = False
        self._counter = -1
        self.max_counter = None
        self.timer = QTimer()
        self.timer.timeout.connect(self.update_counter)
        self.pause_button = None
        self.scalars = None

    def init_anim(self):
        self.scalars = []
        max_time = len(self.ts) * self.dt
        self.max_counter = int(max_time / self.dt)

    @property
    def counter(self):
        return self._counter

    @counter.setter
    def counter(self, value):
        if self._counter == -1:
            self.init_anim()

        self._counter = max(self._counter, 0)
        self._counter = value % self.max_counter
        self.timeout.emit(self._counter, self.max_counter)

    def update_counter(self):
        self.counter += 1

    def left_(self):
        self.counter = 0

    def prec_(self):
        self.counter -= 1

    def prec_10_(self):
        self.counter -= 10

    def play_(self):
        if self._timer:
            self.timer.stop()
            self.pause_button.setIcon(QIcon(tf.f(__file__) / "rsc/play-circle.svg"))
            self._timer = False
        else:
            self.timer.start(self.interval)
            self.pause_button.setIcon(QIcon(tf.f(__file__) / "rsc/pause-circle.svg"))
            self._timer = True

    def next_(self):
        self.counter += 1

    def next_10_(self):
        self.counter += 10

    def right_(self):
        if self.max_counter is None:
            self.init_anim()
        self.counter = self.max_counter - 1


log = logging.getLogger(__name__)
