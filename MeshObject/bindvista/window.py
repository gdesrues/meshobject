import logging

import treefiles as tf
from MeshAnim.time_series import TimeSeries
from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt, QEvent, QSize
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QToolBar, QLineEdit, QPushButton, QAction
from pyvistaqt import MainWindow

from MeshObject.bindvista.animation import Animation
from MeshObject.bindvista.interactor import PvInter


class BaseWindow(MainWindow):
    def __init__(self, size: tuple = None, title: str = None, tb=False, **plotter_cfg):
        super().__init__(size=size, title=title)
        self.bn_ = None
        self.setWindowIcon(QIcon(tf.f(__file__) / "rsc/app.svg"))
        self.tool_bar = None
        self.frame = QtWidgets.QFrame()
        vlayout = QtWidgets.QVBoxLayout()
        vlayout.setContentsMargins(0, 0, 0, 0)

        self.plotter = PvInter(parent=self.frame, **plotter_cfg, set_anim=self.set_anim)
        self.plotter.window_size = size
        vlayout.addWidget(self.plotter.interactor)

        self.mainMenu = self.menuBar()
        self.setStyleSheet(MENU_SS)  # .f(bg=pyvista.global_theme.background))
        self.pb = None  # pause_button for backward linking
        self.anim = None
        if tb:
            self.set_toolbar()
            self.mainMenu.setCornerWidget(self.tool_bar)

        self.frame.setLayout(vlayout)
        self.setCentralWidget(self.frame)

        if not plotter_cfg.get("off_screen", False):
            self.show()

    def set_anim(self, ts: TimeSeries):
        if self.anim is not None:
            return

        self.anim = Animation(ts)
        self.anim.timeout.connect(self.plotter.updt_anim)

        if self.tool_bar is None:
            raise RuntimeError("Cannot set animation without 'tb=True' in Plotter init")
        self.tool_bar.addSeparator()

        # Display simulation time
        te_anim = QLineEdit(self.frame)
        te_anim.setReadOnly(True)
        te_anim.setFixedSize(QSize(200, 19))
        te_anim.setToolTip("Current frame in animation")
        self.tool_bar.addWidget(te_anim)
        if ts.dt is None:
            ts.compute_dt()
        r_ = lambda x: round(x, 2)
        cal = lambda i, j: te_anim.setText(
            f"Time (ms): {r_(i*ts.dt)} / {r_((j-1)*ts.dt)}  ({i+1} / {j}) "
        )
        self.anim.timeout.connect(cal)
        te_anim.setAlignment(Qt.AlignCenter)

        # Animation buttons
        def add_b_(a, b, c):
            btn = self.set_btn(a, b)
            self.tool_bar.addWidget(btn)
            btn.clicked.connect(getattr(self.anim, c))
            return btn

        add_b_("arrow-bar-left", "Go to first itereration", "left_")
        add_b_("chevron-double-left", "Go backward by 10 steps", "prec_10_")
        add_b_("chevron-left-solid", "Go backward", "prec_")
        self.pb = add_b_("play-circle", "Play/Pause the animation", "play_")
        self.pb.setShortcut("space")
        self.bn_ = add_b_("chevron-right-solid", "Go forwrad", "next_")
        add_b_("chevron-double-right", "Go forwrad by 10 steps", "next_10_")
        add_b_("arrow-bar-right", "Go to last itereration", "right_")

        self.anim.pause_button = self.pb
        self.mainMenu.setCornerWidget(self.tool_bar)
        self.bn_.click()  # init

    def event(self, event):
        if event.type() == QEvent.ShortcutOverride:
            if event.key() == Qt.Key_Escape:
                return self.close()
        return super().event(event)

    def set_btn(self, x, txt=None, size=13):
        s1, s2 = size, int(1.5 * size)
        y = QPushButton(self.frame)
        y.setIcon(QIcon(tf.f(__file__) / f"rsc/{x}.svg"))
        y.setIconSize(QSize(s1, s1))
        y.setMinimumSize(QSize(s2 + int(0.5 * s2), s2))
        y.setMaximumSize(QSize(s2 + int(0.5 * s2), s2))
        y.setToolTip(txt)
        return y

    def set_toolbar(self):
        fileMenu = self.mainMenu.addMenu("File")
        exitButton = QAction("Exit (esc)", self)
        exitButton.setShortcut("Ctrl+Q")
        exitButton.triggered.connect(self.close)
        fileMenu.addAction(exitButton)

        # Camera reset
        self.tool_bar = QToolBar(self.frame)
        camera_home_btn = self.set_btn("maximize-solid", "Center camera (r)")
        camera_home_btn.clicked.connect(self.plotter.reset_camera)
        self.tool_bar.addWidget(camera_home_btn)


MENU_SS = tf.Str(
    """
    QMenuBar {
        background-color: rgb(49,49,49);
        color: rgb(255,255,255);
        border: 1px solid #000;
    }

    QMenuBar::item {
        background-color: rgb(49,49,49);
        color: rgb(255,255,255);
    }

    QMenuBar::item::selected {
        background-color: rgb(30,30,30);
    }

    QMenu {
        background-color: rgb(49,49,49);
        color: rgb(255,255,255);
        border: 1px solid #000;           
    }

    QMenu::item::selected {
        background-color: rgb(30, 30, 30);
    }
"""
)

log = logging.getLogger(__name__)
