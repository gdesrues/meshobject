import logging

import numpy as np
import treefiles as tf
from MeshObject import Mesh, Lines


# class BaseActor(Mesh):
#     def __init__(self, data, **kwargs):
#         super().__init__(data, **kwargs)
#
#     def plot(self, **kwargs):
#         kwargs.update({"show_edges": True})
#         super().plot(**kwargs)
from bindvista.pvplot2 import PvPlot2, theme_default
import pyvista as pv

log = logging.getLogger(__name__)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    log = tf.get_logger()

    # m = Mesh.Sphere()
    # a = BaseActor(m)
    # a.plot()

    n = 4
    m_lines = Mesh.from_elements(
        np.random.uniform(0, 1, (n, 3)), [[i, (i + 1) % n] for i in range(n)]
    )

    m0 = Mesh.Sphere(theta_res=10, phi_res=10)
    m_points = Mesh.from_elements(m0.pts)

    # mplane = Mesh.Plane(normal=[1, 0, 0], res_x=1, res_y=1)
    # mp = mplane.copy()
    # mp.transform(scale=(1, 15, 15), translate=(20, 0, 0))

    m_vectors = m_points.copy()
    m_vectors.addPointData(m0.point_normals, "vectors")

    with PvPlot2(theme=theme_default) as p:
        p.show(title="Nice title")
        # p.add_mesh_clip_plane(m0)
        # p.plot_vectors(m_vectors, "vectors", scale=0.25, show_scalar_bar=False, color='green')
        # p.plot_points(m_vectors, point_size=20, color='white')
        # p.plot_mesh(m0, color="#ff7f6d", smooth_shading=True)
        # p.camera_position = cam
        # p.plot_mesh(m0, style='wireframe', render_lines_as_tubes=True, line_width=10)
        # p.remove_scalar_bar()
        # p.show_bounds()
        # def my_plane_func(normal, origin):
        #     slc = m0.pv.slice(normal=normal, origin=origin)
        #     p.add_mesh(slc, name='arrows')
        # p.add_plane_widget(my_plane_func)

        def create_mesh(value):
            res = int(value)
            sphere = pv.Sphere(phi_resolution=res, theta_resolution=res)
            p.add_mesh(sphere, name='sphere', show_edges=True)
            return

        p.add_slider_widget(create_mesh, [5, 100], title='Resolution', pointa=(.1, .9), pointb=(.5, .9))

    # with PvPlot2(shape=(1, 3)) as p:
    #     p.show(title="Nice title")
    #     p[0, 0].plot_vectors(m_vectors, "vectors")
    #     p[0, 1].plot_mesh(m0)
    #     p[0, 2].plot_points(m_points)
    #     for i in range(3):
    #         p[0, i].add_title(f"t-{i}")
