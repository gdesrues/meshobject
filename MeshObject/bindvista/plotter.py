import logging
import sys
import traceback
from dataclasses import dataclass
from typing import Union

import pyvista as pv
from PyQt5 import QtWidgets
from pyvista import themes

from MeshObject.bindvista.interactor import PvInter
from MeshObject.bindvista.window import BaseWindow


class Plotter:
    """
    Main qt-based plotter.

    Press ``echap`` to quit, ``space`` is play/pause if toolbar is enabled (for animations).

    .. code-block::

        with Plotter() as p:
            p.add_mesh(...)
            p.screenshot(...)

    :params size: size of the window
    :params title: title of the window
    :params no_orientation: removes the axes widget
    :param cfg:
        - off_screen: bool to hide the plotting window
        - shape: tuple for subplots
            .. code-block::

                with Plotter(shape=(1,2)) as p:
                    p[0, 0].add_mesh(...)           # plot on left panel
                    p.add_mesh(...)                 # plot on left panel
                    p[0, 1].add_mesh(...)           # plot on right panel
                    p.screenshot(...)

    """
    def __init__(self, size=(1200, 912), title="Title", no_orientation=False, **cfg):
        self.cfg = {**cfg, "title": title, "size": size, "border": False}
        self.no_ori = no_orientation

    def __enter__(self) -> Union[PvInter, pv.Plotter]:
        self.app = None
        self.app = QtWidgets.QApplication(sys.argv)
        self.window = BaseWindow(**self.cfg)
        if not self.no_ori:
            try:
                self.window.plotter.add_camera_orientation_widget()
            except:
                pass
        return self.window.plotter

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is not None:
            log.error(f"{exc_type.__name__}: {exc_val}")
            traceback.print_tb(exc_tb)
        if not self.cfg.get("off_screen", False):
            self.app.exec_()


@dataclass
class Themes:
    """
    Builtin themes

    - PV: ParaViewTheme
    - DOC: DocumentTheme
    - NUM: OnScreenTheme
    """
    PV = themes.ParaViewTheme()
    PV.show_edges = True

    DOC = themes.DocumentTheme()
    DOC.show_edges = True
    DOC.color = "w"
    # DOC.slider_styles.modern.selected_color = "k"

    NUM = pv.themes.DefaultTheme()
    NUM.color = "#ffab6d"
    NUM.show_edges = True
    NUM.edge_color = "k"
    NUM.background = "#ffd36d"
    NUM.nan_color = None
    try:
        NUM.enable_camera_orientation_widget = True
    except:
        pass


log = logging.getLogger(__name__)
