import logging

import numpy as np

from MeshObject.object import Object as Mesh


class Lines(Mesh):
    def to_pyvista(self):
        line = Mesh.pvPD(self.pts)
        line.lines = np.column_stack(([2] * self.nbCells, self.cells.as_np()))
        for x in self.pointDataNames:
            line[x] = self.getPointDataArray(x)
        return line


log = logging.getLogger(__name__)
