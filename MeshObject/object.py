import logging
import os
from dataclasses import dataclass
from typing import List, Union, TypeVar

import numpy as np
import pyvista as pv
import treefiles as tf
import vtk
from vtkmodules.util.numpy_support import vtk_to_numpy, numpy_to_vtk
from vtkmodules.vtkCommonCore import (
    vtkObject,
    vtkIdList,
    vtkPoints,
    vtkDoubleArray,
    mutable,
)
from vtkmodules.vtkCommonDataModel import (
    vtkPolyData,
    vtkPointLocator,
    vtkCellArray,
    vtkDataSet,
    vtkUnstructuredGrid,
    vtkImageData,
    vtkBoundingBox,
    vtkDataObject,
    vtkCellLocator,
)
from vtkmodules.vtkCommonTransforms import vtkTransform
from vtkmodules.vtkFiltersCore import (
    vtkMassProperties,
    vtkCenterOfMass,
    vtkAppendFilter,
)
from vtkmodules.vtkFiltersGeneral import vtkOBBTree
from vtkmodules.vtkFiltersModeling import vtkDijkstraGraphGeodesicPath

from MeshObject.VtkWrapping import Points, Cells, Types, Type, IdList, Cell, CellArray
from MeshObject.convert_old_style import convert_to_old_format, change_vtk_version
from MeshObject.decorators import (
    deprecated,
    unstructured_grid_method,
    vtkFilter,
    polydata_method,
    timer,
)
from MeshObject.mmg_options import MMGOptions
from MeshObject.prefabs import (
    BuildTransform,
    Sphere,
    Cylinder,
    Cube,
    Ellipsoid,
    Superquadric,
    Plane,
)
from MeshObject.utils import transform2numpy, numpy2transform
from MeshObject.vtk_wrap import Reader, Writer

try:
    from cardiac_utils.mesh import SpatialData

    HAS_CARDIAC_UTILS = True
except ImportError:
    HAS_CARDIAC_UTILS = False


def set_SpatialData(sd):
    global HAS_CARDIAC_UTILS, SpatialData
    SpatialData = sd
    HAS_CARDIAC_UTILS = True


TObject = TypeVar("TObject", bound="Object")
TObjectLoadable = Union[TObject, str, vtkObject]


class Object:
    """
    Main vtk interface for object inheriting from vtkDataSet.

    Easy to read/write, apply filters, access data, etc..
    """

    types = Types
    VERTEX = Types.VERTEX
    LINE = Types.LINE
    TRIANGLE = Types.TRIANGLE
    TETRA = Types.TETRA
    POLYDATA = vtkPolyData
    UNSTRUCTUREDGRID = vtkUnstructuredGrid

    X = np.array([1, 0, 0])
    Y = np.array([0, 1, 0])
    Z = np.array([0, 0, 1])

    def __init__(self, data, **kwargs):
        """
        See `Object.load` to instanciate a <gaetools.meshing.Object>.

        :param vtkDataSet data: Internal vtk data
        """
        if isinstance(data, Object):
            data = data.data

        self.data = data
        self.filename = kwargs.get("filename")

        # Built later if necessary
        self.cellsAroundPoints = None
        self._pointIdsAroundPoint = None
        self.pointLocator = None
        self.cellLocator = None
        self.tetras_around_points = None

    def __getattribute__(self, name):
        try:
            getattr(vtk, f"vtk{name}")
        except AttributeError:
            return super().__getattribute__(name)
            # log.critical(f"Filter vtk{name} could not be found in vtk's attributes or an error occured calling cellTypes method/property")
            # log.debug(f"Object: \n{self}")
            # raise AttributeError(f"Object has no attribute {name!r}")

        else:

            def wrapper(**kwargs):
                return self.apply_filter(name, **kwargs)

            return wrapper

    def copy(self):
        """Deep copy of vtkDataSet but shallow copy of attributes"""
        data = type(self.data)()
        data.DeepCopy(self.data)
        obj = type(self)(data)
        # obj.__dict__.update(self.__dict__)
        return obj

    @property
    def type(self):
        """Return the type of the internal vtkDataSet"""
        return type(self.data)

    @classmethod
    def read(cls, filename, **kwargs):
        """
        Read vtkDataSet from file.

        :param str filename: File to read
        :param str type: Type of `vtkReader`, default is `generic`

        .. note::
            You can specify a reader format with type:

            - XMLPolyData | xmlpd
            - PolyData | pd
            - UnstructuredGrid | ugrid
            - GenericDataObject | generic

            eg:

            - obj.read('surf.vtp', type='XMLPolyData')
            - obj.read('vol.vtk', type='ugrid')
        """
        return cls(Reader(filename, **kwargs).data, filename=filename, **kwargs)

    @classmethod
    def load(cls, data, **kwargs):
        """
        Main method to initiliaze a vtk object.

        :param data: Multiple types supported: Object, vtkDataSet, str(path), SpatialData, vtkAlgorithm
        """
        if isinstance(data, str):
            return cls.read(data, **kwargs)
        elif isinstance(data, pv.DataSet):
            with tf.TmpFile(suffix=".vtk") as tmp:
                data.save(tmp.name)
                obj = cls.load(tmp.name)
                obj.filename = None
                return obj
        elif isinstance(data, vtkObject):
            if isinstance(data, vtkDataSet):
                return cls(data, **kwargs)
            else:
                return cls(data.GetOutput())
        elif isinstance(data, cls):
            return data
        elif isinstance(data, Object):
            return cls.load(data.data)
        elif HAS_CARDIAC_UTILS and isinstance(data, SpatialData):
            return cls(data.vtk_polydata)
        else:
            raise RuntimeError(f"Unexpected type {type(data)!r}, cls={cls}")

    def new_empty(self):
        return self.new(self.type)

    @classmethod
    def new(cls, dtype):
        """
        Create instance with empty vtk object.

        :param dtype: vtk class or class name of the wrapped vtk object
        """
        # Workaround because isinstance fails with vtk objects
        if hasattr(dtype, "__name__"):
            if dtype.__name__.startswith("vtk"):
                dtype = dtype.__name__

        if type(dtype) is str:
            dtype = dtype.lower()
            if dtype in ["pd", "polydata", "vtkpolydata"]:
                return cls(vtkPolyData())
            elif dtype in ["ug", "unstructuredgrid", "vtkunstructuredgrid"]:
                return cls(vtkUnstructuredGrid())
            else:
                msg = f"<Object.new> Unexpected type {dtype!r}, choose between pd|ug"
                log.error(msg)
                raise RuntimeError(msg)
        elif isinstance(dtype, vtkObject):
            return cls(dtype())
        else:
            msg = f"<Object.new> Unexpected type {dtype!r}"
            log.error(msg)
            raise RuntimeError(msg)

    @classmethod
    def from_elements(
        cls, vertices, faces=None, dtype="pd", point_arrays=None, filename: str = None
    ):
        """
        Create instance of <Object> with initialized vtk data.

        :param dtype: vtk class or class name of the wrapped vtk object
        :param vertices: Array of vertices topology
        :param faces: Array of faces topology
        """
        obj = cls.new(dtype)
        obj.points = vertices  # from setter
        if faces is not None:
            obj.cells = faces  # from setter
        if point_arrays is not None:
            for name, arr in point_arrays.items():
                obj.addPointData(arr, name)

        if filename:
            obj.filename = filename

        return obj

    @property
    def elems(self):
        return self.points.as_np(), self.cells.as_np()

    @classmethod
    def Sphere(cls, radius=None, center=None, theta_res=None, phi_res=None, **kwargs):
        if radius is not None:
            kwargs.update({"SetRadius": radius})
        if center is not None:
            kwargs.update({"SetCenter": center})
        if theta_res is not None:
            kwargs.update({"SetThetaResolution": theta_res})
        if phi_res is not None:
            kwargs.update({"SetPhiResolution": phi_res})

        return cls(Sphere(**kwargs))

    @classmethod
    def Plane(cls, normal=None, center=None, res_x=None, res_y=None, **kwargs):
        """https://vtk.org/doc/nightly/html/classvtkPlaneSource.html"""
        if normal is not None:
            kwargs.update({"SetNormal": normal})
        if center is not None:
            kwargs.update({"SetCenter": center})
        if res_x is not None:
            kwargs.update({"SetXResolution": res_x})
        if res_y is not None:
            kwargs.update({"SetYResolution": res_y})
        return cls(Plane(**kwargs))

    @classmethod
    def Cube(cls, make_triangles: bool = True, **kwargs):
        """https://vtk.org/doc/nightly/html/classvtkCubeSource.html"""
        # SetBounds, SetCenter
        c = cls(Cube(**kwargs))
        if make_triangles:
            c.Delaunay3D()  # make triangles for gmsh support
            c.convertToPolyData()
        return c

    @classmethod
    def Superquadric(cls, **kwargs):
        return cls(Superquadric(**kwargs))

    @classmethod
    def Cylinder(cls, **kwargs):
        """https://vtk.org/doc/nightly/html/classvtkCylinderSource.html"""
        quad_cylinder = cls(Cylinder(**kwargs))
        quad_cylinder.TriangleFilter()
        quad_cylinder.clean()
        return quad_cylinder

    @classmethod
    def Ellipsoid(cls, radiuses, center=None):
        ell = cls(Ellipsoid(radiuses))
        if center is not None:
            ell.transform(translate=center)
        return ell

    def write(self, *args, support_new_type=True, change_vtk_version=False, **kwargs):
        """
        Write vtkDataSet to file.

        :param str filename: Filename to write, positional argument
        :param str type: Type of `vtkWriter`, default is `generic`

        .. note::
            You can specify a writer format with type:

            - XMLPolyData | xmlpd
            - PolyData | pd
            - UnstructuredGrid | ugrid
            - GenericDataObject | generic

            eg:

            - obj.write('surf.vtp', type='XMLPolyData')
            - obj.write('vol.vtk', type='ugrid')
        """
        if len(args) == 0:
            if self.filename is not None:
                args = [self.filename]
            else:
                log.error("Give a filename")
        Writer(*args, **kwargs, data=self.data)

        # vtk DataFile Version 5.1 generates OFFSETS and CONNECTIVITY
        # support_new_type set to False will convert to the old-style file type, ie
        # CELLS   numPoints0,i,j,k,l,...
        if not support_new_type:
            self.convert_to_old_format(args[0])
        if change_vtk_version:
            self.change_vtk_version(5.1, args[0])

    def write_sofa(self, fname):
        """Write VTK file compatible with SOFA-framework loader"""
        import meshio

        with tf.TmpFile(suffix=".vtk") as f:
            self.write(f.name)
            meshio.write(fname, meshio.read(f.name), file_format="vtk42", binary=False)

    def __repr__(self):
        fname = "" if self.filename is None else f" - {os.path.basename(self.filename)}"
        s = f"\n** {self.__class__.__name__}<{type(self.data).__name__}>{fname} **\n"
        s += f" - {self.nbPoints} points\n"
        s += f" - {self.nbCells} cells\n"
        cellTypes = np.rint(self.cellTypes)
        for t in np.unique(cellTypes):
            nbCells = len(np.where(cellTypes == t)[0])
            name = self.types.find(t).name
            s += f"   - {nbCells} {name}s\n"
        if len(self.pointDataNames) > 0:
            s += f" - PointData: {', '.join([f'{n!r}' for n in self.pointDataNames])}\n"
        if len(self.cellDataNames) > 0:
            s += f" - CellData: {', '.join([f'{n!r}' for n in self.cellDataNames])}\n"
        return s

    @deprecated("Use <Mesh.points> or <Mesh.pts>")
    @property
    def old_points(self):
        """Get all points"""
        return vtk_to_numpy(self.data.GetPoints().GetData())

    @property
    def points(self):
        """Get all points"""
        return Points(self.data.GetPoints())

    @property
    def pts(self) -> np.ndarray:
        """Get all points"""
        return self.points.as_np()

    def point(self, idx: int):
        """Get one point"""
        return np.array(self.data.GetPoint(idx))

    @points.setter
    def points(self, array):
        """Set all points"""
        pts = vtkPoints()
        for i in range(len(array)):
            pts.InsertNextPoint(array[i])
        self.data.SetPoints(pts)

    @property
    def bounds(self):
        """Get the bounds"""
        return self.data.GetBounds()

    # @property
    # def pcells(self):
    #     c = Cells(self.data)
    #     pts = self.points.as_np()
    #     for x in c:
    #         x.points = pts[x.ids]
    #         print(x.points.as_np())
    #         break
    #     for x in c:
    #         print(x.points.as_np())
    #         breakpoint()
    #     return c

    @property
    def cells(self):
        """Get all cells"""
        return Cells(self.data)

    def addCells(self, arr):
        for ids in arr:
            dtype = self.types.from_nb_points(len(ids)).vtk_type
            self.data.InsertNextCell(dtype, IdList.from_np(ids))

    # # @property
    # def numpy_cells(self):
    #     """ Get all cells in a contiguous numpy array
    #     """
    #     return vtk_to_numpy(self.data.GetCells())
    #     # return vtk_to_numpy(self.data.GetPolys())

    @cells.setter
    def cells(self, cells):
        #
        # Better to call InsertNextCell on vtk data, e.g.:
        #     mesh.data.InsertNextCell(dtype.val, IdList.from_np(ids))
        #
        # for example `dtype = Object.TRIANGLE` and `ids = [1,2,3]`
        #
        if isinstance(cells, vtkCellArray):
            # self.data.SetCells(cells)
            new_cells = CellArray(cells)
        elif type(cells) is Cells:
            # self.data.SetCells(cells.data)
            new_cells = cells
        elif isinstance(cells, (np.ndarray, list)):
            new_cells = Cells.from_array(cells)
        else:
            log.critical(f"Unknown type: {type(cells)}")
            raise NotImplementedError

        method = Object.getCells_Method(self.data, p="Set", s="s")
        if self.type is vtkPolyData:
            method(new_cells)
        else:
            # breakpoint()
            method(new_cells.types, new_cells)

    @staticmethod
    def getCells_Method(vtk_obj, p="", s=""):
        method = "Poly" if type(vtk_obj) is vtkPolyData else "Cell"
        method = f"{p}{method}{s}"
        return getattr(vtk_obj, method)

    @property
    @deprecated("Replaced by 'Object.cells', using class <vtk_wrap.Cells>")
    def np_cells(self):
        """Get all cells"""
        return vtk_to_numpy(self.data.GetCells().GetData())

    @property
    def cellTypes(self):
        """
        Return an array of size the number of cells, containing the type of each cell.
        """
        if self.type is vtkPolyData:
            return [c.type.vtk_type for c in self.cells]
        else:
            return vtk_to_numpy(self.data.GetCellTypesArray())

    @property
    def nbPoints(self):
        """Get the number of points"""
        return self.data.GetNumberOfPoints()

    @property
    def nbLines(self):
        """Get the number of points"""
        return len(
            np.where(np.array(self.cellTypes, dtype=int) == self.LINE.vtk_type)[0]
        )

    @property
    def nbCells(self):
        """Get the number of cells"""
        return self.data.GetNumberOfCells()

    def __getitem__(self, item):
        if self.hasPointData(item):
            return self.getPointDataArray(item)
        if self.hasCellData(item):
            return self.getCellDataArray(item)
        raise KeyError(item)

    def __setitem__(self, key, value):
        x = np.array(value)
        if x.shape[0] == self.nbPoints:
            self.addPointData(x, key)
        elif x.shape[0] == self.nbCells:
            self.addCellData(x, key)
        else:
            raise RuntimeError(f"input data does not match the number of elements")

    def interpolate(
        self,
        source,
        fname=None,
        volume=True,
        inside_val=0,
        include=None,
        exclude=None,
        kernel=None,
        dtype=np.float64,
    ):
        """
        Function intended to interpolate PointData between two data sets. `self` will be the
        geometry template; arrays from `source` will be interpolated on this geometry.

        :param source: vtkDataSet object-loadable (see `Object.load`), vtkDataSet with point data arrays to copy
        :param str fname: Output vtkDataSet filename, vtkDataSet with the newly interpolated arrays
        :param bool volume: If true will interpolate pointdata to all points (including inside the surface), False see inside_val
        :param float inside_val: If volume is False, will replace inside pointdata value by inside_val
        :param list include: PointData array names to copy (if None will pass all PointData arrays)
        :param list exclude: PointData array names to not copy
        :param dict kernel: Parameters for the interpolation kernel [vtkGaussianKernel|vtkVoronoiKernel|vtkShepardKernel|vtkSPHKernel].
           See https://vtk.org/doc/nightly/html/classvtkInterpolationKernel.html.
           Possible methods for `vtkGaussianKernel` are `SetKernelFootprintToNClosest`, `SetNumberOfPoints`
        :param np.dtype dtype: numpy type of the PointData array
        """
        if exclude is None:
            exclude = ["Normals"]
        surf_object = self.copy()
        # Remove all cells not on the boundary, but do not clean, ie leaving ghost points
        if not volume:
            surf_object.GeometryFilter(SetMerging=False)

        if kernel is None:
            kernel = dict(
                name="vtkGaussianKernel",
                SetSharpness=4,
                SetKernelFootprintToNClosest=[],
                SetNumberOfPoints=20,
            )
        else:
            kernel = dict(kernel)

        vtkKernel = getattr(vtk, kernel.pop("name"))()
        for k, v in kernel.items():
            if not isinstance(v, list):
                v = [v]
            getattr(vtkKernel, k)(*v)

        # Transport point data to the new mesh points
        surf_object.PointInterpolator(
            SetSourceData=Object.load(source).data, SetKernel=vtkKernel
        )

        # Since polydata is not cleaned, it still contains ghosts points
        # unconnected to cells. All points on the surface have at least one
        # adjacent cell, others are marked as True in inside_points
        inside_points = np.zeros(surf_object.nbPoints, dtype=bool)
        if not volume:
            for i in range(surf_object.nbPoints):
                cells = vtkIdList()
                surf_object.data.GetPointCells(i, cells)
                if cells.GetNumberOfIds() == 0:
                    inside_points[i] = True

        # Actually pass the pointData arrays
        if include is None:
            include = surf_object.pointDataNames

        for name in include:
            orig_arr = surf_object.getPointDataArray(name)
            if name not in exclude:
                orig_arr[inside_points] = inside_val
                assert (
                    orig_arr.shape[0] == self.nbPoints
                ), f"Wrong point data size: '{orig_arr.shape[0]} != {self.nbPoints}'"
                self.addPointData(orig_arr.astype(dtype), name)

        # Write to file
        if fname is not None:
            self.write(fname)

    def add_(self, x):
        xx = ("x", "y", "z")
        self[x] = self.pts[:, xx.index(x)]
        return self

    @polydata_method
    def deleteCells(self, dtype=None, indices=None):
        """Delete cells in mesh"""
        if dtype is not None:
            dtype = Types.find(dtype)
            ct = np.array(self.cellTypes)
            indices = np.where(ct == dtype.vtk_type)[0]
        elif indices is not None:
            pass
        else:
            raise TypeError

        self.data.BuildLinks()
        for i in indices:
            self.data.DeleteCell(i)

        self.data.RemoveDeletedCells()
        log.debug("Cells deleted")

    def filterCellsTo(self, idType: Type):
        """
        Keep only the cells of type `idType`

        :param int idType: vtk cell type, see `the cell types <https://vtk.org/doc/nightly/html/vtkCellType_8h_source.html>`_
        """
        # if isinstance(idType, Type):
        #     idType = idType.vtk_type

        if self.type is Object.POLYDATA and idType == Object.TRIANGLE:
            m = self.copy()
            m.convertToUnstructuredGrid()
            m.filterCellsTo(idType)
            m.convertToPolyData()
            self.data = m.data

        else:
            if self.type is Object.POLYDATA:
                self.convertToUnstructuredGrid()

            cells, types = vtkCellArray(), self.cellTypes
            ids_to_keep, vtk_type = [], idType.vtk_type
            for i, type in enumerate(types):
                if type == vtk_type:
                    if type == 3:
                        breakpoint()
                    cells.InsertNextCell(self.data.GetCell(i))
                    ids_to_keep.append(i)
            if (c_ := cells.GetNumberOfCells()) == 0:
                log.error("No cells found in filtered mesh")
            self.data.SetCells([vtk_type] * c_, cells)

            for cell_data_name in self.cellDataNames:
                array = self.getCellDataArray(cell_data_name)
                self.addCellData(array[ids_to_keep], cell_data_name)

        return self

    @unstructured_grid_method
    def removeCells(self, idType):
        """
        Remove all cells having type `idType`

        :param int idType: vtk cell type, see `the cell types <https://vtk.org/doc/nightly/html/vtkCellType_8h_source.html>`_
        """
        cells, types, myTypes = vtkCellArray(), [], self.cellTypes
        for i, type in enumerate(myTypes):
            if type != idType:
                types.append(type)
                cells.InsertNextCell(self.data.GetCell(i))
        self.data.SetCells(types, cells)

    def cellsType(self, idType):
        """
        Return the id of all the cells of type `idType`

        :param int idType: vtk cell type, see `the cell types <https://vtk.org/doc/nightly/html/vtkCellType_8h_source.html>`_
        """
        return np.where(self.cellTypes == idType)

    # @property
    # @deprecated("")
    # def triangles(self):
    #     return self.np_cells[self.cellsType(vtk.VTK_TRIANGLE)]

    # @property
    # @deprecated("")
    # def tetras(self):
    #     return self.np_cells[self.cellsType(vtk.VTK_TETRA)]

    @property
    @unstructured_grid_method
    def triangles(self):
        return CellArray().from_array(
            self.cells.as_np()[self.cellsType(vtk.VTK_TRIANGLE)]
        )

    @property
    @unstructured_grid_method
    def tetras(self):
        tet = []
        for cell in self.cells:
            if cell.type is Object.types.TETRA:
                tet.append(cell)
        return tet

    @deprecated("")
    @unstructured_grid_method
    def addCell(self, *points_id, **arrays_name):
        """
        Adds a new cell to the mesh defined by `points_id`.
        Cell data is set to 0 unless it is specified as kwargs.

        Example: add a triangle defined by points 50, 51, 52 and with value in array `array_name` equals to 15.23
        ```python
        mesh.addCell(50, 51, 52, array_name=15.23)
        ```

        ..warning:
            Does not apply to polydata!

        :param points_id: Point ids of the new cell
        :param arrays_name: Dict of array name and value
        """
        dtype = self.types.from_nb_points(len(points_id))
        type = dtype.vtk_type
        cell = dtype.new()

        # for i, point_id in enumerate(points_id):
        #     cell[i] = point_id
        cell = Cell(ids=points_id)

        cells = self.data.GetCells()
        cells.InsertNextCell(cell)

        cells_type = self.cellTypes
        cells_type = np.append(cells_type, type)

        if cells_type.size == 1:
            cells_type = cells_type[0]

        self.data.SetCells(cells_type, cells)

        for name in self.cellDataNames:
            arr = self.getCellDataArray(name)
            shp = arr.shape[-1] if arr.ndim > 1 else 1
            arr = np.append(arr, arrays_name.pop(name, np.zeros(shp)))
            self.removeCellData(name)
            self.addCellData(arr, name)

        for k, v in arrays_name.items():
            log.warning(f"Value {v} for array named {k!r} was not used")

    def addPointData(self, data, name):
        """
        Add an array to the PointData arrays

        :param np.array data: Array of size the number of points
        :param str name: Name of the array
        """
        array = numpy_to_vtk(data)
        array.SetName(name)
        self.data.GetPointData().AddArray(array)

    def addCellData(self, data, name):
        """
        Add an array to the CellData arrays

        :param np.array data: Array of size the number of cells
        :param str name: Name of the array
        """
        array = numpy_to_vtk(data)
        array.SetName(name)
        self.data.GetCellData().AddArray(array)

    def has_point_array(self, name):
        """Check if the mesh has the point data ``name``"""
        return self.pointData.HasArray(name)

    def has_cell_array(self, name):
        """Check if the mesh has the cell data ``name``"""
        return self.cellData.HasArray(name)

    def removePointData(self, *names):
        """
        Remove arrays to the PointData arrays

        :param list<str> names: Names of the arrays to remove
        """
        for name in names:
            if not self.pointData.HasArray(name):
                log.debug(f"Array {name!r} could not be found")
            else:
                # log.debug(f"Removing PointDataArray {name!r}")
                self.pointData.RemoveArray(name)

    def removeCellData(self, *names):
        """
        Remove arrays to the CellData arrays

        :param list<str> names: Names of the arrays to remove
        """
        for name in names:
            if not self.cellData.HasArray(name):
                log.debug(f"Array {name!r} could not be found")
            else:
                # log.debug(f"Removing CellDataArray {name!r}")
                self.cellData.RemoveArray(name)

    def clear(self, *keep):
        """Removes all cell and point data arrays"""
        for name in self.pointDataNames:
            if name not in keep:
                self.removePointData(name)
        for name in self.cellDataNames:
            if name not in keep:
                self.removeCellData(name)
        return self

    def hasPointData(self, name):
        """Check if the mesh has the point data ``name``"""
        return name in self.pointDataNames

    def hasCellData(self, name):
        """Check if the mesh has the cell data ``name``"""
        return name in self.cellDataNames

    def getPointDataArray(self, name):
        """Return the array of name `name`"""
        if not self.hasPointData(name):
            msg = (
                f"{name!r} was not found in vtk PointData arrays: {self.pointDataNames}"
            )
            log.error(msg)
            raise RuntimeError(msg)
        return vtk_to_numpy(self.pointData.GetArray(name))

    def getCellScalars(self):
        """Return the cellData scalars"""
        return vtk_to_numpy(self.cellData.GetScalars())

    def getCellDataArray(self, name):
        """Return the array of name `name`"""
        if not self.hasCellData(name):
            msg = f"{name!r} was not found in vtk CellData arrays: {self.cellDataNames}"
            log.error(msg)
            raise RuntimeError(msg)
        return vtk_to_numpy(self.cellData.GetArray(name))

    @property
    def pointDataNames(self):
        """Return the names of the point data arrays"""
        names = list()
        for i in range(self.pointData.GetNumberOfArrays()):
            names.append(self.pointData.GetArrayName(i))
        return names

    @property
    def cellDataNames(self):
        """Return the names of the cell data arrays"""
        names = list()
        for i in range(self.cellData.GetNumberOfArrays()):
            names.append(self.cellData.GetArrayName(i))
        return names

    @property
    def pointData(self):
        """GetPointData"""
        return self.data.GetPointData()

    @property
    def cellData(self):
        """GetCellData"""
        return self.data.GetCellData()

    @vtkFilter
    def apply_filter(self, filter_name, **kwargs):
        """
        Explicit call to a vtk filter.
        Filter can be called without the `vtk`, passing options as `kwargs`.
        """
        return filter_name

    @vtkFilter
    def PointDataToCellData(self, SetPassPointData=True, **kwargs):
        return

    def CustomPointDataToCellData(
        self, point_name, new_name=None, method=np.max, dtype=np.uint8
    ):
        """
        Computes conversion between point to cell data by only considering
        the maximum value at cell vertices
        """
        if new_name is None:
            new_name = point_name

        pt_data = self.getPointDataArray(point_name)
        new_cell_data = np.empty((self.nbCells, *pt_data.shape[1:]), dtype=dtype)
        for i, cell in enumerate(self.cells):
            vals = [pt_data[p_id] for p_id in cell]
            new_cell_data[i] = method(np.array(vals))
        self.addCellData(new_cell_data, new_name)

    def CustomCellDataToPointData(
        self, cell_name=None, new_name=None, method=np.max, scalars=False
    ):
        """
        Computes conversion between cell to point data by only considering
        the maximum value at cell vertices
        """
        if self.cellsAroundPoints is None:
            self.buildCellsAroundPoints()

        if new_name is None:
            new_name = cell_name

        if scalars:
            cell_data = self.getCellScalars()
        else:
            cell_data = self.getCellDataArray(cell_name)
        new_point_data = np.empty(self.nbPoints, dtype=np.uint8)

        for p_id in range(self.nbPoints):
            neighbors = self.cellsAroundPoints[p_id]
            vals = [cell_data[c_id] for c_id in neighbors]
            new_point_data[p_id] = method(np.array(vals))

        self.addPointData(new_point_data, new_name)

    @vtkFilter
    def CellDataToPointData(self, SetPassCellData=True, **kwargs):
        return

    @vtkFilter
    def FillHolesFilter(self, SetHoleSize=100000, **kwargs):
        return

    @vtkFilter
    def GeometryFilter(self, SetMerging=False, **kwargs):
        return

    def convertToPolyData(self, clean=True):
        """Convert a mesh to a PolyData"""
        if self.type is not vtkPolyData:
            self.GeometryFilter(SetMerging=False)
        if clean:
            self.CleanPolyData()
        return self

    @polydata_method
    def convertToUnstructuredGrid(self):
        """Convert a mesh to an UnstructuredGrid"""
        filter = vtkAppendFilter()
        filter.AddInputData(self.data)
        filter.Update()

        ugrid = Object.new("ug")
        ugrid.data.ShallowCopy(filter.GetOutput())
        self.data = ugrid.data

    @classmethod
    def merge(cls, *objs):
        """Merge several meshes in one"""
        filter = vtkAppendFilter()
        for x in objs:
            for y in tf.get_iterable(x):
                filter.AddInputData(y.data)
        filter.Update()
        return cls(filter.GetOutput())

    @polydata_method
    def clean(self):
        """Alias to vtk.vtkCleanPolyData"""
        self.CleanPolyData()
        return self

    def getVolume(self):
        """
        Compute volume of vtkPolyData

        .. warning::
            You may need to close the surface with `vtkFillHolesFilter` or the `close` method
        """
        filter = vtkMassProperties()
        filter.SetInputData(self.data)
        filter.Update()
        return filter.GetVolume()

    def getCenter(self):
        """
        Computes the barycenter. Weights are supported by vtk but not implemented here
        """
        filter = vtkCenterOfMass()
        filter.SetInputData(self.data)
        filter.Update()
        return np.array(filter.GetCenter())

    def intersect(self, point_1, point_2):
        """
        Computes the intersection of the mesh with the line defined by two points.

        :param point_1: First point of the line (list or np.array, of size 3)
        :param point_2: Second point of the line (list or np.array, of size 3)

        :return: Coordinates of n intersection points
        """
        tree = vtkOBBTree()
        tree.SetDataSet(self.data)
        tree.BuildLocator()
        intersectPoints = Points()
        tree.IntersectWithLine(point_1, point_2, intersectPoints, None)
        # log.debug(f"Intersection numPoints: {len(intersectPoints)}")
        return intersectPoints.as_np()

    def buildPointLocator(self):
        self.pointLocator = vtkPointLocator()
        self.pointLocator.SetDataSet(self.data)
        self.pointLocator.BuildLocator()

    def buildCellLocator(self):
        self.cellLocator = vtkCellLocator()
        self.cellLocator.SetDataSet(self.data)
        self.cellLocator.BuildLocator()

    def closestPoint(self, point) -> "PairIdCoord":
        """
        Return the id and coordinates of the closest point to `point` in the mesh

        :param point: Coordinates of a point

        :return: Index and coordinates of nearest mesh point
        """
        if self.pointLocator is None:
            self.buildPointLocator()
        idx = self.pointLocator.FindClosestPoint(point)
        return PairIdCoord(idx, np.array(self.data.GetPoint(idx)))

    def closestCell(self, point) -> "PairIdCoord":
        """
        Return the id of the closest cell to `point` in the mesh

        :param point: Coordinates of a point

        :return: Index of nearest mesh cell
        """
        if self.cellLocator is None:
            self.buildCellLocator()

        closest_point = [0, 0, 0]
        cellId = mutable(0)
        subld = mutable(0)
        dist2 = mutable(0.0)

        self.cellLocator.FindClosestPoint(point, closest_point, cellId, subld, dist2)
        return PairIdCoord(int(cellId), np.array(closest_point))

    @staticmethod
    def barycentric_coords_triangle(point: np.ndarray, triangle: np.ndarray):
        a, b, c = triangle[0], triangle[1], triangle[2]
        v0 = b - a
        v1 = c - a
        v2 = point - a
        d00 = np.dot(v0, v0)
        d01 = np.dot(v0, v1)
        d11 = np.dot(v1, v1)
        d20 = np.dot(v2, v0)
        d21 = np.dot(v2, v1)
        denom = d00 * d11 - d01 * d01
        v = (d11 * d20 - d01 * d21) / denom
        w = (d00 * d21 - d01 * d20) / denom
        u = 1 - v - w
        return u, v, w

    def project_bary_coords(self, point: np.ndarray):
        """Project point on surface using cell locator and then computes the barycentric
        coordinates of the projected point in the closest triangle"""
        res = self.closestCell(point)
        bcoords = Object.barycentric_coords_triangle(
            res.coord, self.pts[self.cells[res.idx].ids]
        )
        return bcoords

    def transform(
        self,
        transform: Union[vtkTransform, np.ndarray] = None,
        vec1=None,
        vec2=None,
        translate=None,
        rotate=None,
        scale=1,
        post_multiply=True,
    ) -> np.ndarray:
        """
        Shorthand method to transform the mesh.

        See `BuildTransform <https://gitlab.inria.fr/gdesrues1/meshobject/-/blob/master/MeshObject/prefabs.py#L27-67>`_.
        """
        if transform is None:
            transform = BuildTransform(
                vec1, vec2, translate, rotate, scale, post_multiply
            )
        elif isinstance(transform, np.ndarray):
            transform = numpy2transform(transform)

        if self.type is vtkPolyData:
            self.TransformPolyDataFilter(SetTransform=transform)
        else:
            self.TransformFilter(SetTransform=transform)

        return transform2numpy(transform)

    def dilate(self, scale):
        origin = np.array(self.BBCenter)
        self.transform(translate=-origin)
        self.transform(scale=scale)
        self.transform(translate=origin)

    scale = dilate  # alias

    def interpolate_to_image(
        self, point_array_name, spacing=np.ones(3), name_valid_mask="vtkValidPointMask"
    ):
        log.warning(
            f"Weird behavior, checked that array_passed is really {point_array_name!r}"
        )
        # array = self.getPointDataArray(point_array_name)
        # self.clear()
        # self.addPointData(array, point_array_name)
        log.debug(f"Interpolate from {self.pointDataNames[0]!r}")

        bounds = self.bounds
        spacing = np.asarray(spacing, np.float64)
        dim = np.array(
            [
                np.ceil((bounds[2 * i + 1] - bounds[2 * i]) / spacing[i])
                for i in range(3)
            ],
            np.int,
        )
        origin = np.array([bounds[2 * i] + spacing[i] / 2 for i in range(3)])

        log.debug(f"{dim[0] * dim[1] * dim[2]} pixels in image")

        img_template = vtkImageData()
        img_template.SetExtent(0, dim[0] - 1, 0, dim[1] - 1, 0, dim[2] - 1)
        img_template.SetSpacing(spacing)
        img_template.SetDimensions(dim)
        img_template.SetOrigin(origin)

        obj = Object(img_template)
        obj.ProbeFilter(
            SetSourceData=self.data, SetValidPointMaskArrayName=name_valid_mask
        )

        # pts = (
        #     probe_filter.GetValidPoints()
        # )  # Get the list of point ids in the output that contain attribute data interpolated from the source.
        # log.debug(f"nbPts interpolated (valid mask): {pts.GetSize()}")

        return obj

    def getBoundingBox(self):
        """
        Computes the bounding box
        """
        box = vtkBoundingBox()
        for p in self.points:
            box.AddPoint(p)
        return box

    def getBoundingBoxCorners(self):
        """
        Returns the bounding box corners
        """
        box = self.getBoundingBox()
        pts = np.zeros((8, 3))
        for i in range(8):
            box.GetCorner(i, pts[i, :])
        return pts

    @property
    def BBDiag(self):
        return self.getBoundingBox().GetDiagonalLength()

    def getBBCenter(self):
        center = np.empty(3)
        self.getBoundingBox().GetCenter(center)
        return center

    @property
    def BBCenter(self):
        return self.getBBCenter()

    @deprecated("Avoid using it")
    def buildTrianglesOnBoundary(self, **array_names):
        """
        Creates a triangulation for points on boundary
        """
        # Convert ugrid to triangle mesh (aka polydata)
        polydata = self.copy()
        polydata.convertToPolyData()

        # Instanciate the point locator on ugrid
        pointLocator = vtkPointLocator()
        pointLocator.SetDataSet(self.data)
        pointLocator.BuildLocator()

        # Get cells and types
        cells = self.data.GetCells()
        cells_type = self.cellTypes

        for cell in polydata.cells:
            # For each triangle on polydata, find the indices of the corresponding vertices on ugrid
            new_cell = Cell(
                ids=[pointLocator.FindClosestPoint(coord) for coord in cell.points]
            )
            cells.InsertNextCell(new_cell)
            cells_type = np.append(cells_type, new_cell.type.val)

        # Set the updated cells array
        # self.data.SetCells(cells_type, cells)
        self.cells = cells

        # Add value in cell arrays to complete the added cells, see the kwargs
        for name in self.cellDataNames:
            if not name in array_names:
                log.warning(
                    f"{name!r} is not touched, file may be corrupted, pass '{name}=value' to add dumb values to cell array"
                )
            else:
                arr = self.getCellDataArray(name)
                shp = arr.shape[-1] if arr.ndim > 1 else 1
                temp_full = np.repeat(
                    array_names.pop(name, np.zeros(shp)), polydata.nbCells
                )
                new_array = np.concatenate((arr, temp_full))
                self.removeCellData(name)
                self.addCellData(new_array, name)

        for unused in array_names:
            log.warning(
                f"{unused!r} does not seem to be used, check that it exists in array"
            )

    def neighbors(self, pointId=None, cellId=None):
        if pointId is not None:
            if self.cellsAroundPoints is None:
                self.buildCellsAroundPoints()

            return self.cellsAroundPoints[pointId]

        if cellId is not None:
            raise NotImplementedError

    def buildCellsAroundPoints(self):
        cellsAroundPoints = [[] for _ in range(self.nbPoints)]
        for cell_id, cell in enumerate(self.cells):
            for pid in cell:
                cellsAroundPoints[pid].append(cell_id)
        self.cellsAroundPoints = np.array(cellsAroundPoints, dtype=object)

    def buildTriangles(self, new_mesh=False, new_cell_data=None):
        mesh = self.copy()
        mesh.filterCellsTo(Object.TETRA)

        triangles = []
        for t in mesh.cells:
            # vtk ordering:  http://www.vtk.org/wp-content/uploads/2015/04/file-formats.pdf
            ts = t.ids
            triangles.append((ts[1], ts[2], ts[0]))
            triangles.append((ts[3], ts[1], ts[0]))
            triangles.append((ts[2], ts[3], ts[0]))
            triangles.append((ts[3], ts[2], ts[1]))

        triangles = np.unique(np.array(triangles), axis=0)

        if new_mesh:
            out_mesh = self.new_empty()
            out_mesh.points = mesh.points
        else:
            out_mesh = mesh

        dtype = self.types.TRIANGLE
        for t in triangles:
            cell = [point_id for i, point_id in enumerate(t)]
            out_mesh.data.InsertNextCell(dtype.val, IdList.from_np(cell))

        # Mesh data
        if new_cell_data:
            new_cell_data = np.ones(triangles.shape[0]) * new_cell_data
            for c in self.cellDataNames:
                arr = np.concatenate((self.getCellDataArray(c), new_cell_data))
                out_mesh.addCellData(arr, c)

            # for p in self.pointDataNames:
            #     out_mesh.addPointData(self.getPointDataArray(p), p)
        else:
            out_mesh.clear()

        return out_mesh

    @timer
    def make_manifold(self, verbose=False, inPlace=True):
        """Reconstruct a manifold clean surface from input mesh.

        Requires pymeshfix

        Parameters
        ----------
        verbose : bool, optional
            Controls output printing.  Default False.
        inPlace : bool, optional
            Update mesh in place or return new instance
        """
        try:
            import pymeshfix
        except ImportError:
            raise ImportError(
                "pymeshfix not installed.  Please run:\n" "pip install pymeshfix"
            )

        if self.type is vtkUnstructuredGrid:
            log.warning(
                "'make_manifold' method only accepts polydata as input, converting now"
            )
            self.convertToPolyData()

        # Run meshfix
        meshfix = pymeshfix.MeshFix(self.points.as_np(), self.cells.as_np())
        if not verbose:
            # TODO fix __enter__ method
            # with suppress_stdout_stderr:
            meshfix.repair()
        else:
            meshfix.repair(verbose)

        # New <Object> containing the manifold surface
        repaired_mesh = Object.new("pd")
        # overwrite this object with cleaned mesh
        repaired_mesh.points = meshfix.v
        repaired_mesh.cells = meshfix.f

        # Interpolate data arrays
        repaired_mesh.interpolate(self.copy())

        # update or return
        if inPlace:
            self.data = repaired_mesh.data
        else:
            return repaired_mesh

    def custom_remove_duplicates(self):
        cells = self.cells.to_numpy()
        u = np.unique(np.sort(cells), axis=0)
        self.cells = u

    @property
    def pointIdsAroundPoint(self):
        if self._pointIdsAroundPoint is None:
            self.buildPointIdsAroundPoint()
        return self._pointIdsAroundPoint

    def getPointsAroundPoint(self, point_id):
        return self.pointIdsAroundPoint[point_id]

    def buildPointIdsAroundPoint(self):
        ids_around_point = [[] for _ in range(self.nbPoints)]
        for c in self.cells:
            for p in c:
                ids_around_point[p].extend(c.ids)

        if len(min(ids_around_point, key=lambda x: len(x))) == 0:
            msg = f"Some points are not connected to any cell, you may clean your mesh"
            log.critical(msg)
            raise InvalidMesh(msg)

        for p in range(self.nbPoints):
            pp = list(dict.fromkeys(ids_around_point[p]))
            pp.remove(p)
            ids_around_point[p] = pp

        self._pointIdsAroundPoint = ids_around_point
        return ids_around_point

    @unstructured_grid_method
    def buildTetrasAroundPoint(self):
        self.tetras_around_points = [[] for _ in range(self.nbPoints)]
        for cell in self.tetras:
            for pid in cell:
                self.tetras_around_points[pid].append(cell.id)

    @unstructured_grid_method
    def tetrasAroundPoint(self, p_id):
        if self.tetras_around_points is None:
            self.buildTetrasAroundPoint()
        return self.tetras_around_points[p_id]

    @polydata_method
    def smooth(self, nb_iter=20, relax=0.1):
        self.SmoothPolyDataFilter(
            SetNumberOfIterations=nb_iter, SetRelaxationFactor=relax
        )
        return self

    def surface_geodesic(self, array_name=None, set_start_coord=None, **kwargs):
        """
        Compute the geodesic distance from ``set_start_coord`` to any point on the mesh.

        :param array_name: array_name created
        :param set_start_coord: distance=0
        :param kwargs: See https://vtk.org/doc/nightly/html/classvtkDijkstraGraphGeodesicPath.html.
            Ex: SetStartVertex, SetEndVertex, StopWhenEndReachedOn

        """
        if self.type is not vtkPolyData:
            log.warning(
                f"{Object.surface_geodesic} is intended for PolyData only, converting to polydata"
            )
            self.convertToPolyData()

        if set_start_coord is not None:
            i = self.closestPoint(set_start_coord).idx
            kwargs.update({"SetStartVertex": i})

        filter = vtkDijkstraGraphGeodesicPath()
        filter.SetInputData(self.data)

        for k, v in kwargs.items():
            if not isinstance(v, list):
                v = [v]
            getattr(filter, k)(*v)

        filter.Update()

        distance_map = vtkDoubleArray()
        filter.GetCumulativeWeights(distance_map)
        distance_map = vtk_to_numpy(distance_map)

        if array_name is not None:
            self.addPointData(distance_map, array_name)

        return distance_map

    def getVoronoi(
        self, seed_points, mesh=None, method=None, array_name=None, recenter=False
    ):
        from MeshObject.voronoi import Voronoi

        if mesh is None:
            mesh = self.copy()

        v = Voronoi(mesh=mesh, seed_points=seed_points)
        array = v.run(method)

        if recenter:
            v.recenter_points()

        if array_name is not None:
            self.addPointData(array, array_name)

        return v

    def getVoronoi_Lloyd(self, nbPoints, mesh=None):
        """
        Returns the ids of the voronoi centers on the mesh
        """
        from MeshObject.voronoi import PointCloudUtils

        if mesh is None:
            mesh = self.copy()

        v = PointCloudUtils(mesh)
        seeds_id = v.Llyod(nbPoints)
        return seeds_id

    def threshold(self, btw, array_name, method=None, to_polydata=True):
        if method is None:
            method = "point"

        # if method is not "point" or method is not "cell":
        #     raise NotImplemented

        self.Threshold(
            ThresholdBetween=[btw[0], btw[1]],
            SetInputArrayToProcess=[
                0,
                0,
                0,
                getattr(vtkDataObject, f"FIELD_ASSOCIATION_{method.upper()}S"),
                array_name,
            ],
        )
        if self.nbPoints == 0:
            log.warning("Threshold may have failed, mesh has 0 points")

        if to_polydata:
            self.convertToPolyData()

        return self

    @property
    def point_normals(self):
        if self.nbCells == 0:
            log.error(f"The mesh has 0 cells, cannot call 'TriangleMeshPointNormals'")
        normals_filter = self.copy()
        normals_filter.TriangleMeshPointNormals()
        assert normals_filter.hasPointData(
            "Normals"
        ), f"'point_normals' failed for {self}"
        return normals_filter.getPointDataArray("Normals")

    @property
    @polydata_method
    def cell_normals(self):
        normals_filter = self.copy()
        normals_filter.PolyDataNormals()
        return normals_filter.getPointDataArray("Normals")

    @timer
    @unstructured_grid_method
    def addTriangles(self, replace=False, val_added_cells=0):
        """
        Builds all triangles in the unstructured mesh
        :param replace: stack cells (default) or replace cells array
        :param val_added_cells: if replace is False, default value for the new cells in cell data arrays
        """
        log.info("Building triangles")
        self.filterCellsTo(Object.types.TETRA)

        tri, tri_sorted = [], []
        for cell in self.cells:
            c = cell.as_np()
            for j in range(4):
                a, b, d = c[j % 4], c[(j + 1) % 4], c[(j + 2) % 4]
                tri.append([a, b, d])
                tri_sorted.append(sorted([a, b, d]))
        #         tri.append(sorted([c[j % 4], c[(j + 1) % 4], c[(j + 2) % 4]]))
        # tri = np.unique(np.array(tri), axis=0)
        # Global convention for nodes indexing is not lost, sorted triangle vertices indexes are kept apart from non sorted ones
        _, indexes = np.unique(np.array(tri_sorted), axis=0, return_index=True)
        tri = np.array(tri)[indexes]

        if replace:
            self.cells = tri
        else:
            self.addCells(tri)
            ept = np.ones(tri.shape[0]) * val_added_cells
            for cd in self.cellDataNames:
                new_arr = np.concatenate((self.getCellDataArray(cd), ept))
                self.addCellData(new_arr, cd)

    # @unstructured_grid_method
    # def cleanUGrid(self):
    #     import paraview.simple as psimple
    #     import paraview.servermanager as pserver
    #
    #     tp = psimple.TrivialProducer()
    #     tp.GetClientSideObject().SetOutput(self.data)
    #
    #     psimple.CleantoGrid(tp)
    #     psimple.CleanCellstoGrid(tp)
    #
    #     self.data = pserver.Fetch(tp)

    def mmg_options(
        self, hausd=None, hgrad=None, hmin=None, hmax=None, nr: bool = None
    ):
        return MMGOptions(
            mesh=self, hausd=hausd, hgrad=hgrad, hmin=hmin, hmax=hmax, nr=nr
        )

    @polydata_method
    def tetrahedralize(
        self, mmg_opt: [List, MMGOptions] = None, inplace=True, **kwargs
    ) -> TObject:
        """
        Remeshing with mmg. Check in particular `-hausd` and `-hgrad` parameters.
        See more in the Tetrahedralize class.
        """
        from MeshObject.poly_meshing import Tetrahedralize

        if self.type is Object.POLYDATA:
            self.clean()

        kwargs.update({"mmg_opt": mmg_opt})
        with tf.TmpFile(suffix=".vtk") as mesh:
            Tetrahedralize(self.data, mesh.name, **kwargs)
            obj = Object.read(mesh.name, type="ugrid")
        obj.filterCellsTo(Object.TETRA)
        if inplace:
            self.data = obj.data
            return self
        else:
            return obj

    def remesh(self, sol: [np.ndarray, List], inplace: bool = True, mmg_opt=None):
        """
        Uses mmg to remesh a tetrahedral mesh with a metric defined by `sol`.
        For the moment, only the desired edge lenght at each vertex is supported (isotropic)

        :param sol: np.ndarray or List with self.nbPoints rows and 1 column
        """
        from MeshObject.poly_meshing import remesh
        import meshio

        if self.type is self.UNSTRUCTUREDGRID:
            ct = self.TETRA
            method = "volume"
        elif self.type is self.POLYDATA:
            self.convertToUnstructuredGrid()
            ct = self.TRIANGLE
            method = "surface"
        else:
            raise NotImplementedError

        self.filterCellsTo(ct)

        sol = np.array(sol)
        n = self.nbPoints
        assert n == sol.shape[0], "Sol must have as values as the number of vertices"

        if sol.ndim > 1:
            # Anisotropic metric,
            # see https://www.mmgtools.org/mmg-remesher-try-mmg/mmg-remesher-tutorials/anisotropic-metric-prescription
            log.debug("Anisotropic metric detected")
            if sol[0, ...].ndim == 1:
                _sol, sol_type = [], 2
                for i in range(n):
                    row = [sol[i, 0], sol[i, 1], sol[i, 2]]
                    _sol.append(" ".join(map(str, row)))
            elif sol[0, ...].ndim == 2:
                # assert sol[0, ...].ndim == 2, f"You must give a 3x3 anisotropic tensor"
                _sol, sol_type = [], 3
                for i in range(n):
                    row = [
                        sol[i, 0, 0],
                        sol[i, 0, 1],
                        sol[i, 1, 1],
                        sol[i, 0, 2],
                        sol[i, 1, 2],
                        sol[i, 2, 2],
                    ]
                    _sol.append(" ".join(map(str, row)))
                else:
                    raise TypeError
        else:
            _sol, sol_type = sol.astype(str), 1

        sol = "\n".join(_sol)
        # see https://www.math.u-bordeaux.fr/~dobrzyns/logiciels/Docmmg_english.pdf
        sol_tmpl = (
            "MeshVersionFormatted 2\n"
            "Dimension 3\n"
            f"SolAtVertices {n}\n"
            f"1 {sol_type}\n"
            f"{sol}\n"
            "End"
        )

        with tf.TmpDir() as d:
            d.file(
                inp="input.mesh",
                tmp="input.vtk",
                out="output.vtk",
                out_msh="output.msh",
                sol="metric.sol",
            )
            with open(d.sol, "w") as f:
                f.write(sol_tmpl)
            self.write(d.tmp)
            meshio.read(d.tmp).write(d.inp)
            if mmg_opt is None:
                mmg_opt = MMGOptions()
            mmg_options = mmg_opt.opt + ["-sol", d.sol]
            remesh(d.inp, d.out_msh, mmg_opt=mmg_options, method=method)
            meshio.read(d.out_msh).write(d.out)
            m = Object.load(d.out)
            m.filterCellsTo(ct)

            for x in m.pointDataNames:
                if "sol:metric" in x:
                    y = m.getPointDataArray(x)
                    m.clear()
                    m.addPointData(y, "metric")
                    break

        if inplace:
            self.data = m.data
            return self
        else:
            return m

    @polydata_method
    def triangulate(self, mmg_opt: [List, MMGOptions] = None, inplace=True, **kwargs):
        from MeshObject.poly_meshing import Triangulate

        if self.type is Object.POLYDATA:
            self.clean()

        kwargs.update({"mmg_opt": mmg_opt})
        with tf.TmpFile(suffix=".vtk") as mesh:
            Triangulate(self.data, mesh.name, **kwargs)
            obj = Object.read(mesh.name, type="pd")
        obj.filterCellsTo(Object.TRIANGLE)
        obj.convertToPolyData()
        if inplace:
            self.data = obj.data
        else:
            return obj

    def convert_to_old_format(self, fname: str = None):
        if fname is None:
            fname = self.filename
        convert_to_old_format(fname)

    def change_vtk_version(self, v: float, fname: str = None):
        if fname is None:
            fname = self.filename
        change_vtk_version(fname, v)

    @classmethod
    def marching_cubes(
        cls,
        image: "Image",
        isovalue=0,
        smooth=True,
        make_manifold=False,
        manifold_verbose=False,
    ):
        """
        Takes a <cardiac_utils.infoarray.Image> image and returns an Object<vtkPolyData>
        """
        try:
            from skimage import measure
        except ImportError:
            raise ImportError(
                "You need to install skimage to run the marching cubes method (pip install scikit-image)"
            )

        # Deal with image touching border of the bounding box
        # 1. Add a thin layer (padding) of 1 voxel
        # origin = image.origin - [1, 1, 1]
        # image = np.pad(image, 1)
        # 2. Do nothing and fill holes later

        origin = image.origin
        vertices, faces, normals, values = measure.marching_cubes(
            image.transpose((2, 1, 0)), level=isovalue
        )

        polydata = Object.from_elements(vertices, faces, dtype="pd")
        polydata.transform(translate=origin)
        polydata.FillHolesFilter()

        # values[values == 2] = 0  # Match legend color for further use in Pipeline
        # polydata.addPointData(values, "Ventricles")
        polydata.addPointData(values, "Values")
        polydata.addPointData(normals, "Normals")

        if make_manifold:
            # log.info("Start repairing (approx. 7min)")
            polydata.make_manifold(verbose=manifold_verbose)
        else:
            pass
            # log.info("No manifold check !")

        if smooth:
            polydata.smooth(nb_iter=40, relax=0.1)

        return polydata

    def to_image(self, spacing=None, filename=None, mode=None, bounds=None):
        """
        Wrapper of ``SpatialData.to_image``.

        | See <cardiac_utils.mesh.SpatialData.to_image(
        |    spacing, filename=None, inval=1, outval=0, bounds=None
        | )>

        Return: <cardiac_utils.infoarray.Image> image

        mode: (None (np.bool, default) | "nan" | "-1")

        """
        if not HAS_CARDIAC_UTILS:
            raise ImportError(
                "The cardiac_utils package has not been found "
                "(see https://gitlab.inria.fr/epione/cardiac-epyone)"
            )

        if spacing is None:
            spacing = (1, 1, 1)

        if bounds is None:
            bounds = self.bounds

        sph = SpatialData(vtk_polydata=self.data)
        img = sph.to_image(spacing, filename=filename, bounds=bounds)

        if mode is None:
            return img
        elif mode == "nan":
            imc = img.astype(np.float64)
            imc[np.where(imc == 0)] = np.nan
            return imc
        elif mode == "-1":
            imc = img.astype(np.float64)
            imc[np.where(imc == 0)] = -1
            return imc

    def get_boundary_edges(self, mesh_for_ids=None, rev=True):
        from MeshObject.boundary_edges import getBoundaryEdges

        return getBoundaryEdges(self, mesh_for_ids=mesh_for_ids, rev=rev)

    def plot(self, scalars=None, **kwargs):
        from MeshObject.bindvista.plotter import Plotter

        with Plotter() as p:
            p.add_mesh(self, scalars=scalars, **kwargs)
        # return self.pv.plot(**kwargs)

    def to_pyvista(self):
        return pv.wrap(self.data)

    @property
    def pv(self):  # -> pyvista.DataSet
        return self.to_pyvista()

    @staticmethod
    def pvPD(*a, **k):
        return pv.PolyData(*a, **k)

    def edges_info(self):
        m = self.copy()
        m.clear()
        m.ExtractEdges()
        pts = m.points.as_np()

        lenghts = np.empty(m.nbCells)
        for i, ce in enumerate(m.cells):
            p = [pts[x] for x in ce]
            lenghts[i] = np.linalg.norm(p[1] - p[0])

        log.info(
            f"{lenghts.shape[0]} edges, mean={lenghts.mean()}, std={lenghts.std()}"
        )
        # fig, ax = plt.subplots(nrows=2)
        # ax[0].hist(lenghts, 200, density=True)
        # ax[1].boxplot(lenghts, vert=False)
        return lenghts

    @polydata_method
    def geodesic(self, id1: int, id2: int):
        return pv.wrap(self.data).geodesic(id1, id2)

    def distance_from_surface(self, x):
        """
        Compute distance from each point coord in x to current surface
        """
        from vtkmodules.vtkFiltersCore import vtkImplicitPolyDataDistance

        x = np.asarray(x)
        assert x.ndim == 2  # list of points coordinates
        assert x.shape[1] == 3  # 3D coordinates

        if self.type != Object.POLYDATA:
            self.convertToPolyData()

        imp = vtkImplicitPolyDataDistance()
        imp.SetInput(self.data)

        distances = np.array([imp.EvaluateFunction(c) for c in x])
        distances[np.where(np.abs(distances) < 1e-5)] = 0
        return distances

    def compute_curvature(self, name=None, method="max", **kwargs):

        _d = {
            "gaussian": ("Gaussian", 0),
            "mean": ("Mean", 1),
            "max": ("Maximum", 2),
            "min": ("Minimum", 3),
        }
        assert method in _d

        if self.type is not self.POLYDATA:
            self.convertToPolyData()

        self.Curvatures(SetCurvatureType=_d[method][1], **kwargs)
        # self.plot()

        s = f"{_d[method][0]}_Curvature".replace("ian", "")
        arr = self.getPointDataArray(s)

        if name is not None:
            for x in self.pointDataNames:
                if "_Curvature" in x:
                    self.pointData.RemoveArray(x)
            self.addPointData(arr, name)

        return arr

    @classmethod
    def from_boundary_edges(cls, be, mesh_ids):
        pts = mesh_ids.points.as_np()
        points, faces = [], []
        order = [e[0] for hole in be for e in hole]

        for hole in be:
            for e in hole:
                points.append(pts[e[0]])
                faces.append((order.index(e[0]), order.index(e[1])))

        return cls.from_elements(points, faces)

    def clip_at_height(self, height, axis="z", **kwargs):
        cor = ("x", "y", "z")
        assert axis in cor, f"{axis} not in {cor}"
        i = cor.index(axis)
        pts = self.pts[:, i]
        a, b = self.bounds[i * 2], self.bounds[i * 2 + 1]
        th = a + height * (b - a)
        arr = np.zeros(self.nbPoints)
        arr[np.where(pts < th)] = 1
        self.addPointData(arr, "clip_at_height")
        self.threshold((0.5, 1.5), "clip_at_height", **kwargs)
        self.removePointData("clip_at_height")
        return self

    def remove_aside_triangles(self):
        cells_to_rm = [len(self.neighbors(i)) for i in range(self.nbPoints)]
        self.addPointData(cells_to_rm, "th")
        self.threshold((1.5, 1e10), "th")

    def __eq__(self, other: TObject):
        assert isinstance(other, Object)
        if self.nbPoints != other.nbPoints:
            return False

        if self.nbCells != other.nbCells:
            return False

        return np.array_equal(self.pts, other.pts)

    def __ne__(self, other):
        return not self.__eq__(other)

    def close(self):
        # the 'make_manifold' might close the mesh as well
        m = self.copy()
        m.filterCellsTo(self.TRIANGLE)
        pts = m.pts
        bedges = m.get_boundary_edges()
        new_pts, new_tris = pts.tolist(), m.cells.as_np().tolist()
        for hole in bedges:
            cent = np.zeros(3)
            for e in hole:
                cent += pts[e[0]]
            cent /= len(hole)
            new_pts.append(cent)
            idx = len(new_tris)
            for e in hole:
                new_tris.append((*e, idx))

        m.cells = new_tris
        m.points = new_pts

        return m

    def clip(self, normal="x", origin=None, invert=False, value=0.0):
        clp = self.pv.clip(normal=normal, origin=origin, invert=not invert, value=value)
        obj = type(self).load(clp)
        obj.convertToPolyData()
        obj.TriangleFilter()
        return obj


class InvalidMesh(Exception):
    pass


@dataclass
class PairIdCoord:
    """
    Dataclass representing a point in a mesh defined by:
        - ``idx``: the index in the mesh
        - ``coord``: the coordinates
    """

    idx: int
    coord: np.ndarray

    def __iter__(self):
        return iter((self.idx, self.coord))

    def __getitem__(self, item):
        return (self.idx, self.coord)[item]


log = logging.getLogger(__name__)
