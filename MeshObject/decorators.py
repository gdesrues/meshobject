import functools
import inspect
import logging
import time
import vtk


def timer(func):
    """Print the runtime of the decorated function"""

    @functools.wraps(func)
    def _timer_(*args, **kwargs):
        start_time = time.perf_counter()
        value = func(*args, **kwargs)
        end_time = time.perf_counter()
        run_time = end_time - start_time
        if run_time < 60:
            log.info(f"Finished {func.__name__!r} in {run_time:.4f} secs")
        else:
            log.info(
                f"Finished {func.__name__!r} in {int(run_time/60)} min {int(run_time%60)} secs"
            )
        return value

    return _timer_


def deprecated(msg=""):
    def rdeprecated(func):
        @functools.wraps(func)
        def _deprecated_(*args, **kwargs):
            log.warning(
                f"Deprecated: '{func.__module__}.{func.__name__}' will be removed. {msg}"
            )
            value = func(*args, **kwargs)
            return value

        return _deprecated_

    return rdeprecated


def vtkFilter(func):
    @functools.wraps(func)
    def _vtkFilter_(*args, **kwargs):
        filter = None
        try:
            filter = getattr(vtk, f"vtk{func.__name__}")()
        except:
            filter = func(*args, **kwargs)
            if type(filter) is str:
                try:
                    filter = getattr(vtk, f"vtk{filter}")()
                except:
                    filter = None
        finally:
            if filter is None:
                filterName = args[1] if len(args) > 0 else func.__name__
                log.error(f"Vtk does not have the filter vtk{filterName}")
                raise AttributeError(f"Vtk does not have the filter vtk{filterName}")

        parent = args[0]
        filter.SetInputData(parent.data)

        argspec = inspect.getargspec(func)
        if argspec.defaults is not None:
            positional_count = len(argspec.args) - len(argspec.defaults)
            defaults = dict(zip(argspec.args[positional_count:], argspec.defaults))
            kwargs = {**defaults, **kwargs}

        for k, v in kwargs.items():
            if not isinstance(v, list):
                v = [v]
            getattr(filter, k)(*v)

        filter.Update()
        parent.data = filter.GetOutput()

    return _vtkFilter_


def polydata_method(func):
    @functools.wraps(func)
    def wrapper_polydata_method(*args, **kwargs):
        # args_repr = [repr(a) for a in args]
        # kwargs_repr = [f"{k}={v!r}" for k, v in kwargs.items()]
        # signature = ", ".join(args_repr + kwargs_repr)
        # ({signature})

        if args[0].type is not vtk.vtkPolyData:
            msg = f"In <{func.__module__}.{func.__name__}>, bad object type. Dataset is a {args[0].type.__name__!r} but a 'vtkPolyData' is required"
            log.critical(msg)
            raise TypeError(msg)
        else:
            value = func(*args, **kwargs)
            return value

    return wrapper_polydata_method


def unstructured_grid_method(func):
    @functools.wraps(func)
    def wrapper_unstructured_grid_method(*args, **kwargs):
        if args[0].type is not vtk.vtkUnstructuredGrid:
            msg = f"In <{func.__module__}.{func.__name__}>, bad object type. Dataset is a {args[0].type.__name__!r} but a 'vtkUnstructuredGrid' is required"
            log.critical(msg)
            raise TypeError(msg)
        else:
            value = func(*args, **kwargs)
            return value

    return wrapper_unstructured_grid_method


log = logging.getLogger(__name__)
