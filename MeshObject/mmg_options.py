class MMGOptions:
    def __init__(
        self, mesh=None, hausd=None, hgrad=None, hmin=None, hmax=None, nr: bool = None
    ):
        if mesh is not None and hausd is None:
            from MeshObject.object import Object

            hausd = 0.01 * Object.load(mesh).BBDiag

        self.hausd = hausd
        self.hgrad = hgrad
        self.hmin = hmin
        self.hmax = hmax
        self.nr = nr

    @property
    def opt(self):
        opt = []
        for x in ["hausd", "hgrad", "hmin", "hmax"]:
            v = getattr(self, x)
            if v is not None:
                opt.extend([f"-{x}", v])
        for x in ["nr"]:
            if getattr(self, x):
                opt.append(f"-{x}")
        return opt
