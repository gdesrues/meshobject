import logging
import numpy as np

from MeshObject.decorators import timer
from MeshObject.object import Object


class Voronoi:
    def __init__(self, mesh, seed_points):
        """
        Build a voronoi tesselation from seed_points
        :param mesh: object-loadable data
        :param seed_points: list of points id in mesh
        """
        self.mesh = Object.load(mesh)
        self.points = np.array(seed_points)
        self.region = None

        self.init_voronoi()

    def init_voronoi(self):
        self.region = np.ones(self.mesh.nbPoints) * np.nan
        for i, p_id in enumerate(self.points):
            self.region[p_id] = i

    def grow_1(self):
        neighbors = self.mesh.pointIdsAroundPoint

        for p_id, r in enumerate(self.region):
            if not np.isnan(r):
                for adj_point in neighbors[p_id]:
                    if np.isnan(self.region[adj_point]):
                        self.region[adj_point] = r

    def run(self, method=None):
        if method is None:
            method = "grow"

        if method == "grow":
            while np.isnan(self.region).any():
                self.grow_1()
            return self.region
        else:
            raise NotImplemented

    @timer
    def recenter_points(self):
        regions = self.region

        for i, r in enumerate(np.unique(regions)):
            points_not_in_region = np.where(regions != r)
            array = self.mesh.volume_geodesic(init_nodes=points_not_in_region)
            self.points[i] = np.argmax(array)

        return self.points


class PointCloudUtils:
    def __init__(self, mesh):
        self.mesh = Object.load(mesh)

    def Llyod(self, nbPoints):
        """
        Returns the ids of the voronoi centers on the mesh
        """
        try:
            import point_cloud_utils as pcu
        except ImportError:
            raise ImportError(
                "Package 'point_cloud_utils' not found, visit https://github.com/fwilliams/point-cloud-utils"
            )

        nbPoints = int(nbPoints)
        if nbPoints == 0:
            return []

        coords = pcu.sample_mesh_lloyd(
            self.mesh.points.as_np(), self.mesh.cells.as_np(), nbPoints
        )
        if nbPoints == 1:
            coords = [coords]

        local_ids = []
        for coord in coords:
            i, _ = self.mesh.closestPoint(coord)
            local_ids.append(i)
        return local_ids


log = logging.getLogger(__name__)
