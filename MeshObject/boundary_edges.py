import logging

import numpy as np

from MeshObject import Object
from MeshObject.VtkWrapping import CellArray


def getBoundaryEdges(endo, mesh_for_ids=None, rev=True):
    """ """
    boundary_edges = []
    endo = endo.copy()

    if mesh_for_ids is None:
        mesh_for_ids = endo.copy()

    endo.FeatureEdges(
        SetBoundaryEdges=True, SetFeatureEdges=False, SetNonManifoldEdges=False
    )
    # endo.plot()
    endo.PolyDataConnectivityFilter(
        ColorRegionsOn=[],
        SetExtractionModeToAllRegions=[],
    )
    points = endo.points
    rId = endo.getPointDataArray("RegionId")
    nb_holes = len(np.unique(rId))
    log.debug(f"Found {nb_holes} hole{'s' if nb_holes > 1 else ''}")

    lines = CellArray(endo.data.GetLines()).as_np()
    for i in range(nb_holes):
        couples = []
        for line in lines:
            if rId[line[0]] == i and rId[line[1]] == i:
                couples.append(
                    [mesh_for_ids.closestPoint(points[p_id])[0] for p_id in line]
                )

        boundary_edges.append(chain(couples, rev=rev))

    return boundary_edges


def chain(couples, rev=True):
    if rev:
        seed = couples[0][0]
        ordered_couples = [list(reversed(couples.pop(0)))]
    else:
        seed = couples[0][1]
        ordered_couples = [couples.pop(0)]

    ite, ite_max = 0, len(couples) + 1
    while len(couples) > 0:
        ite += 1
        for i, c in enumerate(couples):
            if c[0] == seed:
                seed = c[1]
                ordered_couples.append(couples.pop(i))
            elif c[1] == seed:
                seed = c[0]
                ordered_couples.append(list(reversed(couples.pop(i))))

        assert ite < ite_max, f"Could not find a unique path,\ncouples: {couples}\nordered_couples: {ordered_couples}"

    return ordered_couples


log = logging.getLogger(__name__)
