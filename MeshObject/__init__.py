from MeshObject.object import Object, TObject, TObjectLoadable

# from MeshObject.child_class import Lines


Mesh = Object
TMesh = TObject
TMeshLoadable = TObjectLoadable

# try:
from MeshObject.bindvista.plotter import Plotter, Themes

# except Exception as e:
#     print('error: from MeshObject.bindvista.plotter import Plotter, Themes')
#     print(e)
