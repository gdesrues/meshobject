import numpy as np
from vtkmodules.vtkCommonMath import vtkMatrix4x4
from vtkmodules.vtkCommonTransforms import vtkTransform


def transform2numpy(t: vtkTransform) -> np.ndarray:
    """
    Get a vtkTransform and get the matrix as a numpy array
    """
    trans = t.GetMatrix()
    trans_arr = np.eye(4)
    trans.DeepCopy(trans_arr.ravel(), trans)
    return trans_arr


def numpy2transform(arr: np.ndarray) -> vtkTransform:
    """
    Get a 4x4 matrix and return the associated vtkTransform
    """
    vmatrix = vtkMatrix4x4()
    vmatrix.DeepCopy(arr.ravel())
    t = vtkTransform()
    t.SetMatrix(vmatrix)
    return t
