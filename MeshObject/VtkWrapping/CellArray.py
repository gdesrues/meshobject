import logging
import numpy as np
from MeshObject.VtkWrapping import IdList, Cell

import vtk


# noinspection PyUnresolvedReferences
class CellArray(vtk.vtkCellArray):
    """
    See https://vtk.org/doc/nightly/html/classvtkCellArray.html for full list of usefull methods
    """

    def __init__(self, vtkCellArray=None):
        if vtkCellArray is None:
            vtkCellArray = vtk.vtkCellArray()
        self.DeepCopy(vtkCellArray)

    @property
    def types(self):
        for ids in self:
            yield vtkTypes.from_nb_points(len(ids))

    def __getitem__(self, i):
        ids = IdList.IdList()
        self.GetCellAtId(i, ids)
        return Cell(ids=ids, cell_id=i)

    def __iter__(self):
        self.InitTraversal()
        for i in range(len(self)):
            ids = IdList.IdList()
            self.GetNextCell(ids)
            yield Cell(ids=ids, cell_id=i)

    def __len__(self):
        return self.GetNumberOfCells()

    @classmethod
    def from_array(cls, arr):
        c = vtk.vtkCellArray()
        c.Allocate(len(arr))
        c.InitTraversal()
        [c.InsertNextCell(IdList.IdList.from_np(ids)) for ids in arr]
        return CellArray(c)

    def as_np(self):
        n, m = len(self), self.GetMaxCellSize()
        cells = -np.ones((n, m), dtype=np.int)
        for i, ids in enumerate(self):
            for j, idx in enumerate(ids):
                cells[i, j] = idx
        return cells

    def __str__(self):
        arr = self.as_np()
        s = f"** CellArray<vtkCellArray>, shape={arr.shape} **\n"
        for l in arr[:3, :]:
            s += f"{l}\n"
        s += "...\n"
        for l in arr[-3:, :]:
            s += f"{l}\n"
        return s


log = logging.getLogger(__name__)
