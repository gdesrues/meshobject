import logging
from MeshObject.VtkWrapping.CellArray import CellArray

import vtk


class Cells(CellArray):
    def __init__(self, vtkDataSet):
        self.vtkDataSet = vtkDataSet

        if type(vtkDataSet) is vtk.vtkPolyData:
            self.__cells_methods = ["Polys", "Lines"]
        else:  # elif type(vtkDataSet) is vtk.vtkUnstructuredGrid:
            self.__cells_methods = ["Cells"]

        self.__getters = [getattr(vtkDataSet, f"Get{k}") for k in self.__cells_methods]

        arr = vtk.vtkCellArray()
        for get in self.__getters:
            arr.Append(get())

        super().__init__(arr)


log = logging.getLogger(__name__)
