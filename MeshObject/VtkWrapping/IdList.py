import logging
import numpy as np

import vtk


# noinspection PyUnresolvedReferences
class IdList(vtk.vtkIdList):
    """
    See https://vtk.org/doc/nightly/html/classvtkIdList.html for full list of usefull methods
    """

    def __init__(self, vtkIdList=None):
        if vtkIdList is None:
            vtkIdList = vtk.vtkIdList()
        self.DeepCopy(vtkIdList)

    @classmethod
    def from_np(cls, arr):
        c = cls(vtk.vtkIdList())
        c.Allocate(len(arr))
        [c.InsertNextId(a) for a in arr]
        return c

    def __len__(self):
        return self.GetNumberOfIds()

    def __getitem__(self, i):
        if i == len(self):
            raise StopIteration
        return self.GetId(i)

    def __str__(self):
        return f"<IdList(vtkIdList) ids={self.as_np()}>"

    def as_np(self):
        return np.array([i for i in self])

    @property
    def legacy(self):
        c = vtk.vtkIdList()
        c.Allocate(len(self))
        [c.InsertNextId(a) for a in self]
        return c


log = logging.getLogger(__name__)
