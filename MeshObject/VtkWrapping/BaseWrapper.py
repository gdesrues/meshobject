import logging

import vtk


def getWrapper(dtype):
    class Wrapper(dtype):
        def __init__(self, init_data=None):
            self.parent = dtype

            if init_data is None:
                init_data = self.parent()

            self.DeepCopy(init_data)

    return Wrapper


class Cell(getWrapper(vtk.vtkGenericCell)):
    def __init__(self, init_data=None):
        super().__init__(init_data)


log = logging.getLogger(__name__)
