import logging

import numpy as np
import vtk
from MeshObject.VtkWrapping.BaseWrapper import getWrapper
from MeshObject.VtkWrapping.IdList import IdList
from MeshObject.VtkWrapping.Points import Points
from MeshObject.VtkWrapping.types import Types, Type


class Cell(getWrapper(vtk.vtkGenericCell)):
    """
    See https://vtk.org/doc/nightly/html/classvtkGenericCell.html
    """

    def __init__(self, init_data=None, ids=None, cell_id=None, dtype: Type = None):
        self.id = cell_id  # self.id is local to the cell array it comes from
        if ids is not None:
            if not dtype:
                dtype = Types.from_nb_points(len(ids))
            super().__init__(dtype.new_vtk())
            self.Initialize()
            self.SetPointIds(IdList.from_np(ids).legacy)
        else:
            super().__init__(init_data)

    @classmethod
    def new(cls, dtype):
        """
        Create new empty cell

        :param dtype: Either a <Type>, or a vtk.vtkType (ex. vtk.VTK_TETRA),
            or the vtk type id (5 for triangles)
        """
        return cls(Types.find(dtype).new_vtk())

    # Moved to __init__
    # @classmethod
    # def from_ids(cls, ids):
    #     dtype = Types.from_nb_points(len(ids))
    #     c = cls(dtype.new_vtk())
    #     c.Initialize()
    #     c.SetPointIds(IdList.from_np(ids).legacy)
    #     return c

    def __len__(self):
        return self.GetNumberOfPoints()

    def __str__(self):
        s = f"<Cell(vtkGenericCell), type={self.type}{f', id={self.id}' if self.id is not None else ''}\n"
        s += f"  - ids: {self.ids}\n"
        s += f"  - points: {self.points}>"
        return s

    def __iter__(self):
        for i in range(len(self)):
            yield self.GetPointId(i)

    def as_np(self):
        return IdList(self.GetPointIds()).as_np()

    @property
    def points(self):
        # Cells does not always hold their point coordinates, the connectivity is sufficient
        # Point coordinates are set to [0,0,0] if not available
        return Points(self.GetPoints())

    @points.setter
    def points(self, pts):
        self.SetPoints(Points.from_array(pts))

    @property
    def ids(self):
        return IdList(self.GetPointIds())

    @property
    def type(self):
        return Types.from_nb_points(len(self))

    @property
    def center(self):
        return np.mean(self.points.as_np(), axis=0)


log = logging.getLogger(__name__)
