import logging
import vtk


class Type:
    """
    Wrapper of a vtkType
    """

    def __init__(self, name, val, vtk_type, nb_points):
        self.name = name
        self.val = val
        self.vtk_type = vtk_type
        self.nb_points = nb_points

    def isA(self, ident):
        return self.name == ident or self.val == ident or self.vtk_type == ident

    def __repr__(self):
        return f"{self.name.capitalize()} ({self.val})"

    def new(self, ids=None):
        """
        Returns a <vtk_wrap.Cell> cell, see new_vtk to get a vtk.vtkCell
        """
        from MeshObject.VtkWrapping.Cell import Cell

        return Cell(ids=ids, init_data=self.new_vtk(), dtype=self)

    def new_vtk(self):
        """
        Returns a new instance of a vtkCell
        """
        return getattr(vtk, f"vtk{self.name.capitalize()}")()


# noinspection PyUnresolvedReferences
class Types:
    """
    Helper for vtk types
    """

    VERTEX = Type("vertex", 1, vtk.VTK_VERTEX, 1)
    LINE = Type("line", 3, vtk.VTK_LINE, 2)
    TRIANGLE = Type("triangle", 5, vtk.VTK_TRIANGLE, 3)
    TETRA = Type("tetra", 10, vtk.VTK_TETRA, 4)
    types = [VERTEX, LINE, TRIANGLE, TETRA]

    @staticmethod
    def find(ident):
        """
        Finds the type based on the name, vtk class or vtk identifier
        """
        if type(ident) == Type:
            return ident
        if type(ident) is str:
            ident = ident.lower()
        for dtype in Types.types:
            if dtype.isA(ident):
                return dtype
        msg = f"Cell with identity {ident} is not recognized"
        log.critical(msg)
        raise TypeError(msg)

    @staticmethod
    def from_nb_points(n):
        """
        Finds the type based on the number of points
        """
        for dtype in Types.types:
            if dtype.nb_points == n:
                return dtype
        msg = f"Cell with {n} points is not recognized"
        print(msg)
        raise RuntimeError(msg)


log = logging.getLogger(__name__)
