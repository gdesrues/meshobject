import logging
import numpy as np

import vtk


# noinspection PyUnresolvedReferences
class Points(vtk.vtkPoints):
    """
    Wrapper for vtkPoints, in pythonic list style
    """

    def __init__(self, vtkPoints=None):
        if vtkPoints is None:
            vtkPoints = vtk.vtkPoints()
        self.DeepCopy(vtkPoints)

    def __str__(self):
        return f"<Points(vtkPoints), size={len(self)}>"

    @classmethod
    def zeros(cls, n):
        c = cls(vtk.vtkPoints())
        c.SetNumberOfPoints(n)
        [c.SetPoint(i, [0, 0, 0]) for i in range(n)]
        return c

    @classmethod
    def from_array(cls, arr):
        arr = np.asarray(arr)
        c = cls.zeros(arr.shape[0])
        [c.SetPoint(i, arr[i]) for i in range(arr.shape[0])]
        return c

    def __len__(self):
        return self.GetNumberOfPoints()

    def __iter__(self):
        for i in range(len(self)):
            yield self.GetPoint(i)

    def __getitem__(self, i):
        return np.array(self.GetPoint(i))

    def __setitem__(self, i, val):
        return self.SetPoint(i, val)

    def as_np(self):
        pts = np.empty((len(self), 3))
        for i, p in enumerate(self):
            pts[i, :] = p
        return pts

    # @classmethod
    # def from_array(cls, points):
    #     c = cls(vtk.vtkPoints())
    #     points = np.asarray(points)
    #     for i in range(points.shape[0]):
    #         self.append(points[i])

    # def append(self, *items):
    #     for item in items:
    #         self.InsertNextPoint(item)


log = logging.getLogger(__name__)
