import logging

import treefiles as tf
import vtk


class VtkInterface:
    """
    IO interface for vtk writer/reader Use type keyword to choose a format
    """

    EASY_ACCESS = dict(
        xmlpd="XMLPolyData",
        pd="PolyData",
        ugrid="UnstructuredGrid",
        xmlugrid="XMLUnstructuredGrid",
        generic="GenericDataObject",
        stl="STL",
        obj="OBJ",
        ply="PLY",
    )
    IO = dict(r="Reader", w="Writer")

    @staticmethod
    def getMethod(key, io):
        key, io = (
            VtkInterface.EASY_ACCESS.get(key.lower(), key),
            VtkInterface.IO.get(io.lower(), ""),
        )
        return getattr(vtk, f"vtk{key}{io}")

    def __init__(self, io=None, type="generic", **kwargs):
        self.obj = VtkInterface.getMethod(type, io)()
        if kwargs.get("mode") is not None:
            assert kwargs["mode"].lower() in ("ascii", "binary", "appended")
            getattr(self.obj, f"SetDataModeTo{kwargs['mode'].capitalize()}")()


class Reader(VtkInterface):
    def __init__(self, filename, **kwargs):
        with open(filename, "rb") as f:
            if b"<" in f.readline() and not "xml" in kwargs.get("type", ""):
                # log.warning(
                #     f"{filename!r} seems to be a xml file, specify type=['xmlpd'|'XMLPolyData'] "
                #     "to read explicitly read with vtkXMLPolyDataReader.\nAdding xml reading option."
                # )
                kwargs["type"] = "xmlpd"

        if "type" not in kwargs:
            if filename.endswith(".obj"):
                kwargs["type"] = "obj"
            elif filename.endswith(".stl"):
                kwargs["type"] = "stl"
            elif filename.endswith(".ply"):
                kwargs["type"] = "ply"

        super().__init__("r", **kwargs)
        self.obj.SetFileName(filename)

    @property
    def data(self):
        self.obj.Update()
        return self.obj.GetOutput()


class Writer(VtkInterface):
    def __init__(self, filename, data=None, connection=None, **kwargs):

        if "type" not in kwargs:
            if filename.endswith(".obj"):
                kwargs["type"] = "obj"
            elif filename.endswith(".stl"):
                kwargs["type"] = "stl"
            elif filename.endswith(".ply"):
                kwargs["type"] = "ply"

        super().__init__("w", **kwargs)
        self.obj.SetFileName(filename)
        # self.obj.SetFileVersion(42)
        if data is not None:
            self.obj.SetInputData(data)
        elif connection is not None:
            self.obj.SetInputConnection(connection)
        else:
            raise RuntimeError("Please give input data for writer")
        # log.debug(f"Writing file://{filename}")
        self.obj.Write()


log = logging.getLogger(__name__)
