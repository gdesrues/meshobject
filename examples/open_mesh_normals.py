import logging

import numpy as np
import treefiles as tf
from MeshObject import Mesh, Plotter


def get_boundary(clip, clip_edges, center):
    _pts, _nor = [], []
    _t_id, _t_ce = [], []
    pts = clip.pts
    N = lambda x: x / np.linalg.norm(x)
    for edge in clip_edges[0]:
        e = pts[edge]
        m = np.mean([*e, center], axis=0)  # triangle center
        n = N(np.cross(e[1] - e[0], center - e[0]))  # normal
        _pts.append(m)
        _nor.append(n * np.linalg.norm(e[1] - e[0]))
        _t_ce.append((len(_t_id), len(_t_id) + 1, len(_t_id) + 2))
        _t_id.extend((*e, center))
    normals = Mesh.from_elements(_pts)
    normals["n"] = _nor
    # Mean normal
    mean_normal = np.mean(_nor, axis=0)
    new_triangles = Mesh.from_elements(_t_id, _t_ce)
    return normals, mean_normal, new_triangles


def main():
    # 1
    m_cyl = Mesh.Cylinder(SetResolution=30, SetRadius=0.5)
    m_cyl.triangulate(m_cyl.mmg_options(hmin=0.3))
    cyl_clip = m_cyl.copy().clip_at_height(0.7, axis="y")

    # 2
    cyl_clip_e = cyl_clip.get_boundary_edges(cyl_clip)
    cyl_clip_be = Mesh.from_boundary_edges(cyl_clip_e, cyl_clip)
    center = np.mean(cyl_clip_be.pts, axis=0)
    normals, mean_n, new_triangles = get_boundary(cyl_clip, cyl_clip_e, center)

    # 3
    clipped = m_cyl.copy().clip(mean_n, center)

    # 4
    clip_tri = clipped.copy()
    clip_tri.triangulate(clipped.mmg_options(hmax=0.2, hmin=0.2, hgrad=1))
    clip_tri_e = clip_tri.get_boundary_edges(clip_tri)
    center_tri = np.mean(Mesh.from_boundary_edges(clip_tri_e, clip_tri).pts, axis=0)
    _, _, new_triangles_tri = get_boundary(clip_tri, clip_tri_e, center_tri)

    nn = dict(render=False)
    nn1 = {
        **nn,
        "glyph_kw": {"scale": "n", "factor": 1},
        "color": "g",
        "show_edges": False,
    }
    nn2 = {
        **nn,
        "direction": mean_n,
        "scale": 0.2,
        "show_edges": False,
        "color": "red",
    }
    with Plotter(
        title="Open Mesh Normals", shape=(2, 2), size=(1000, 1000), no_orientation=True
    ) as p:
        p[0, 0].add_mesh(cyl_clip, **nn)
        p.add_mesh(m_cyl, style="wireframe", **nn)
        p[0, 1].add_mesh(cyl_clip, **nn)
        p.add_mesh(new_triangles, opacity=0.7, color="w", **nn)
        p.add_mesh(cyl_clip_be, **nn, radius=0.01, color="red")
        p.add_point(center, point_color="red")
        p.add_vectors(normals, "n", **nn1)
        p.add_vector(center, **nn2)

        p[1, 0].add_mesh(clipped)
        p[1, 1].add_mesh(clip_tri)
        p.add_mesh(new_triangles_tri, opacity=0.7, color="w", **nn)
        p.add_point(center_tri, point_color="red")

        p.link_views()
        p.camera_position = [
            (-0.04863143892398933, 0.990063400560043, 2.7407731163708897),
            (-0.01350484607116853, -0.04821050862565051, 0.018210984375857435),
            (-0.0038834017245889096, 0.934337517279314, -0.3563682407221121),
        ]
        p.render()
        p.screenshot("src/open_mesh_normals.png")


log = logging.getLogger(__name__)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    log = tf.get_logger()

    main()
