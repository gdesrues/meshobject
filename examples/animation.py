import logging

import numpy as np
import treefiles as tf
from MeshAnim import Animate, TimeSeries, Frames

from MeshObject import Mesh, Plotter


def main(force=False):
    root = tf.f(__file__, "out")

    # Generate data
    with root.ct("meshes") as ct:
        if tf.greedy_download(ct / "mesh.vtk", force=force):
            m = Mesh.Cube(SetBounds=(0, 1, 0, 1, 0, 5), SetCenter=[0, 0, 2.5])
            m.tetrahedralize(m.mmg_options(hmin=0.25, hmax=0.25, hgrad=1))
            m.write(ct.mesh)
            plane = Mesh.Plane((0, 0, 1), (0, 0, 0))
            plane.transform(scale=(2, 2, 1))
            plane.write(ct / "plane.vtk")

        if tf.greedy_download(ct / "serie/meta.series", force=force):
            T = np.linspace(0, 2 * np.pi, 30)
            A = np.cos(T) / 2 + 0.5
            ts = TimeSeries()
            for i, (t, a) in enumerate(zip(T, A)):
                m1 = m.copy()
                pts = m1.pts
                pts[:, 2] *= a
                m1.points = pts
                ts.add_file(t, ct.path(f"serie/mesh_{i}.vtk"), m1)
            ts.write(ct.meta)

    with root.ct("meshes") as ct:
        log.info(ct)
        plane = Mesh.load(ct.plane)
        ts = TimeSeries.from_meta(ct.meta)
        log.info(ts)

    with Plotter(tb=True, title="animation example") as p:
        p.add_ts(ts)
        p.add_mesh(plane, show_edges=False, color="r")
        p.camera_position = [
            (11.28491165889473, 4.288824503270507, 5.787555052506267),
            (0.5, 0.5, 2.5),
            (-0.260802241304335, -0.09152095610326434, 0.9610442786493136),
        ]


def make_movie():
    root = tf.f(__file__, "out")

    gen = Frames(
        camera=[
            (11.28491165889473, 4.288824503270507, 5.787555052506267),
            (0.5, 0.5, 2.5),
            (-0.260802241304335, -0.09152095610326434, 0.9610442786493136),
        ],
    )
    gen.set_options(show_edges=True)
    # gen.set_scalar_bar_options(color="black")
    with root.ct("meshes") as ct:
        ct.dir("images").file(tmp="im_{}.png")
        gen.add_src(
            TimeSeries.from_meta(ct.meta),
            # scalars="timer (s)",
            # clim=ts.range("timer (s)"),
            callback=add_text_callback,
        )
        gen.generate_images(ct.path(ct.tmp))

        an = Animate(ct / "movie.mp4")

        an.add_ax(ct.path(ct.tmp.f("*")))
        an.animate(fps=20, figsize=(8, 8))


def add_text_callback(p, t, m):
    p.add_text(f"time: {round(t, 2)}ms", color="black", font_size=10)


log = logging.getLogger(__name__)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    log = tf.get_logger()

    # main()
    make_movie()
