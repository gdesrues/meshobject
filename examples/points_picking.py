import logging

import numpy as np
import treefiles as tf

from MeshObject import Mesh, Plotter
from MeshObject.labelling.label_hand import select_points


"""
Select some points on a mesh with pyvista cell selection tools
"""


def main():
    SCALARS, LABEL = "test", np.pi
    m = Mesh.Sphere()
    m.tetrahedralize(m.mmg_options(hmax=0.1, nr=True))
    m[SCALARS] = np.zeros(m.nbPoints)

    pts = select_points(m, SCALARS)

    if len(pts) > 0:
        m[SCALARS][pts] = LABEL

    with Plotter(title="Labels after selection") as p:
        p.add_mesh(m, scalars=SCALARS)

    # m.write()


log = logging.getLogger(__name__)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    log = tf.get_logger()

    main()
