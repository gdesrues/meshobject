# Examples

Try out the notebooks in this directory! They cover most of the methods implemented in this package.
You can start with [this notebook](https://gitlab.inria.fr/gdesrues1/meshobject/-/blob/master/examples/notebooks/01_misc.ipynb).



## Animations

![](anim_gif.gif)

To plot an animation: [see here](https://gitlab.inria.fr/gdesrues1/meshobject/-/blob/master/examples/animation.py#L41-48).


To save an animation to a mp4 file: [see here](https://gitlab.inria.fr/gdesrues1/meshobject/-/blob/master/examples/animation.py#L51-76).

## Others


**[Boundary Edges](https://gitlab.inria.fr/gdesrues1/meshobject/-/blob/master/examples/boundary_edges.py)**
![](src/example.png)


**[Clipping validation](https://gitlab.inria.fr/gdesrues1/meshobject/-/blob/master/examples/clipping_validation.py)**
![](src/clipping_validation.png)


**[Open Mesh Normal and Clipping](https://gitlab.inria.fr/gdesrues1/meshobject/-/blob/master/examples/open_mesh_normals.py)**
![](src/open_mesh_normals.png)


### Other - Boundary edges
```python
m = Object.load("<polydata>")
edges_array = m.get_boundary_edges()

edges = Object.from_boundary_edges(edges_array, m)
edges.write(...)
```

![](src/bedges.png)






### Points selection
![](../MeshObject/labelling/p_selection.png)
![](../MeshObject/labelling/p_selected.png)