import logging

import numpy as np
import treefiles as tf

from MeshObject import Object


def main():
    # Create a simple sphere
    m = Object.Sphere(radius=10, theta_res=50, phi_res=10)
    m2 = m.copy()

    # Add elevation sign point data
    z = np.array([m.points.as_np()[:, 2] < 0], dtype=int)
    m.addPointData(z.squeeze(), "z")

    # Cut the sphere
    m.threshold((0.5, 1.5), "z")
    m.write(root.path("hemisphere_1.vtk"))

    # Export the edges of the hemisphere
    edges_array = m.get_boundary_edges()
    edges = Object.from_boundary_edges(edges_array, m)
    edges.write(root.path("boundary_edges_1.vtk"))

    # With remeshing
    m2.triangulate(m2.mmg_options(hmax=1, hmin=1, hgrad=1, nr=True))
    z = np.array([m2.points.as_np()[:, 2] < 0], dtype=int)
    m2.addPointData(z.squeeze(), "z")

    m2.threshold((0.5, 1.5), "z")
    m2.write(root.path("hemisphere_2.vtk"))

    edges_array = m2.get_boundary_edges()
    edges = Object.from_boundary_edges(edges_array, m2)
    edges.write(root.path("boundary_edges_2.vtk"))


log = logging.getLogger(__name__)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    log = tf.get_logger()

    root = tf.fTree(__file__, "out", dump=True)

    main()
