import logging

import numpy as np
import treefiles as tf

from MeshObject import Mesh, Plotter


def main():
    m1 = Mesh.Sphere()
    m1["data"] = np.ones(m1.nbPoints) * 12
    m2 = Mesh.Sphere(SetCenter=Mesh.X)
    m2["data"] = np.ones(m2.nbPoints) * 7
    with Plotter() as p:
        p.add_mesh(Mesh.merge(m1, m2), scalars="data")


log = logging.getLogger(__name__)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    log = tf.get_logger()

    main()
