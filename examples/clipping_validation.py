import logging

import treefiles as tf

from MeshObject import Mesh, Plotter


def main():
    m1 = Mesh.Sphere()
    m2 = Mesh.Cylinder(SetCenter=[1, 0, 0])
    m = Mesh.merge(m1, m2)
    cc = m1.BBCenter + [0.3, 0.1, 0]
    n = [0.5, 1, 0]
    clipped = m.clip(n, cc)
    plane = Mesh.Plane(n, cc, SetPoint1=(1.5, 0, 0), SetPoint2=(0, 1.5, 0))
    clip_save = clipped.copy()

    clipped.PolyDataConnectivityFilter(
        # SetExtractionModeToClosestPointRegion=[], SetClosestPoint=cc
        SetExtractionModeToLargestRegion=[]
    )
    bedges = clipped.get_boundary_edges()
    me = Mesh.from_boundary_edges(bedges, clipped)

    nn = dict(render=False)
    with Plotter(shape=(1, 3), size=(1600, 600), title="Clipping Validation") as p:
        p[0, 0].add_mesh(m, **nn)
        p[0, 1].add_mesh(clip_save, **nn)
        p.add_mesh(plane, **nn, color="w", opacity=0.4)
        p.add_vector(cc, direction=n, color="w", scale=0.4)
        p[0, 2].add_mesh(clipped, **nn)
        p.add_mesh(me, **nn, color="red", radius=0.01)
        p.add_point(cc, **nn, point_color="red")
        p.add_points(me, **nn, color="g", point_size=10)

        p.link_views()
        p.camera_position = [
            (1.8144940959040334, 2.119982112464344, 4.044267395247974),
            (0.5221848542366643, -0.00619544330955215, -0.0018281460855174708),
            (-0.10310923437125884, 0.8936843925211053, -0.4366883240385021),
        ]
        p.render()
        p.screenshot("src/clipping_validation.png")


log = logging.getLogger(__name__)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    log = tf.get_logger()

    main()
