import logging

import numpy as np
import treefiles as tf
import pyvista as pv

from MeshObject import Mesh


def main():
    m = Mesh.load("mesh.vtk")

    # m.addPointData(m.pts[:, 2], "z")
    # m.threshold(
    #     (m.bounds[4] - 10, m.bounds[4] + 0.75 * (m.bounds[5] - m.bounds[4])), "z"
    # )

    m.CellDataToPointData()
    y = m.getPointDataArray("fibers")
    yan = m.getPointDataArray("angles")
    m.clear()
    mesh = pv.wrap(m.data)
    mesh["fibers"] = y
    mesh["angles"] = yan
    mesh = mesh.clip(
        "z",
        origin=[0, 0, m.bounds[4] + 0.75 * (m.bounds[5] - m.bounds[4])],
        invert=True,
    )

    streamlines = mesh.streamlines(
        vectors="fibers",
        source_center=m.BBCenter,
        source_radius=65,
        n_points=15000,
        progress_bar=True,
    )

    p = pv.Plotter()
    # p.add_mesh(src)
    # p.add_mesh(mesh)
    p.add_mesh(streamlines.tube(radius=0.5), scalars="angles")
    p.show()
    p.close()


log = logging.getLogger(__name__)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    log = tf.get_logger()

    main()

    # nx = 20
    # ny = 15
    # nz = 5
    #
    # origin = (-(nx - 1) * 0.1 / 2, -(ny - 1) * 0.1 / 2, -(nz - 1) * 0.1 / 2)
    # mesh = pv.UniformGrid((nx, ny, nz), (0.1, 0.1, 0.1), origin)
    # x = mesh.points[:, 0]
    # y = mesh.points[:, 1]
    # z = mesh.points[:, 2]
    # vectors = np.empty((mesh.n_points, 3))
    # vectors[:, 0] = np.sin(np.pi * x) * np.cos(np.pi * y) * np.cos(np.pi * z)
    # vectors[:, 1] = -np.cos(np.pi * x) * np.sin(np.pi * y) * np.cos(np.pi * z)
    # vectors[:, 2] = (
    #     np.sqrt(3.0 / 3.0) * np.cos(np.pi * x) * np.cos(np.pi * y) * np.sin(np.pi * z)
    # )
    #
    # mesh["vectors"] = vectors
    #
    # stream, src = mesh.streamlines(
    #     "vectors",
    #     return_source=True,
    #     # terminal_speed=0.0,
    #     # n_points=200,
    #     # source_radius=0.1,
    # )
    # print(stream)
    #
    # # cpos = [(1.2, 1.2, 1.2), (-0.0, -0.0, -0.0), (0.0, 0.0, 1.0)]
    # # stream.tube(radius=0.0015).plot(cpos=cpos)
